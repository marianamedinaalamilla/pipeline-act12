<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('shockthesystem');
});

Route::get('/offline', [LaravelPWA\Http\Controllers\LaravelPWAController::class, 'offline']);

Auth::routes(['register' => false, 'verify' => true]);

Route::group(['middleware' => 'auth', 'middleware' => 'verified'], function() {
	Route::post('/notificacion-push', [App\Http\Controllers\PushController::class, 'store']);
	Route::post('/notificacion-baja', [App\Http\Controllers\PushController::class, 'destroy']);
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
	Route::view('usuarios', 'livewire.users.index');
	Route::view('cuatrimestres', 'livewire.cuatrimestres.index');
	Route::post('/cuatrimestres/pouchdb', [App\Http\Controllers\CuatrimestreController::class, 'store']);
	Route::view('carreras', 'livewire.carreras.index');
	Route::view('carrera-cuatrimestres', 'livewire.carrera-cuatrimestres.index');
	Route::view('aulas', 'livewire.aulas.index');
	Route::view('asignaturas', 'livewire.asignaturas.index');
	Route::view('grupo-asignaturas', 'livewire.grupo-asignaturas.index');
	Route::view('grupos', 'livewire.grupos.index');
	Route::view('sensores', 'livewire.sensores.index');
	Route::view('sensor-grupos', 'livewire.sensor-grupos.index');
	Route::view('docentes', 'livewire.docentes.index');
	Route::view('docente-asignaturas', 'livewire.docente-asignaturas.index');
	Route::view('estudiantes', 'livewire.estudiantes.index');
	Route::view('estudiante-grupos', 'livewire.estudiante-grupos.index');
	Route::view('estudiante-asistencias', 'livewire.estudiante-asistencias.index');
	Route::view('notificacion-aulas', 'livewire.notificacion-aulas.index');
});
