const CACHE_STATIC_NAME  = "pwa-static-app_shell-v" + new Date().getTime();
const CACHE_INMUTABLE_NAME = "pwa-inmutable-v" + new Date().getTime();
const CACHE_DYNAMIC_NAME  = "pwa-dynamic-v" + new Date().getTime();
const CACHE_DYNAMIC_LIMIT = 50;

let APP_SHELL = [
    '/offline',
    '/css/uteams.css',
    '/js/uteams.js',
    '/css/sesion.css',
    '/css/menu.css',
    '/js/menu.js',
    '/js/alertas.js',
    '/js/offline.js',
    '/js/bd.js',
    '/js/navigator.js',
    '/js/push.js',
    '/js/oscuro.js',
    '/img/logos/16x16_claro_uteams.png',
    '/img/logos/24x24_claro_uteams.png',
    '/img/logos/32x32_claro_uteams.png',
    '/img/logos/48x48_claro_uteams.png',
    '/img/logos/64x64_claro_uteams.png',
    '/img/logos/72x72_claro_uteams.png',
    '/img/logos/96x96_claro_uteams.png',
    '/img/logos/128x128_claro_uteams.png',
    '/img/logos/144x144_claro_uteams.png',
    '/img/logos/152x152_claro_uteams.png',
    '/img/logos/171x82_claro_uteams.png',
    '/img/logos/192x192_claro_uteams.png',
    '/img/logos/256x256_claro_uteams.png',
    '/img/logos/384x384_claro_uteams.png',
    '/img/logos/512x512_claro_uteams.png',
    '/img/logos/16x16_oscuro_uteams.png',
    '/img/logos/24x24_oscuro_uteams.png',
    '/img/logos/32x32_oscuro_uteams.png',
    '/img/logos/48x48_oscuro_uteams.png',
    '/img/logos/64x64_oscuro_uteams.png',
    '/img/logos/72x72_oscuro_uteams.png',
    '/img/logos/96x96_oscuro_uteams.png',
    '/img/logos/128x128_oscuro_uteams.png',
    '/img/logos/144x144_oscuro_uteams.png',
    '/img/logos/152x152_oscuro_uteams.png',
    '/img/logos/171x82_oscuro_uteams.png',
    '/img/logos/192x192_oscuro_uteams.png',
    '/img/logos/256x256_oscuro_uteams.png',
    '/img/logos/384x384_oscuro_uteams.png',
    '/img/logos/512x512_oscuro_uteams.png',
    '/img/splashs/640x1136.png',
    '/img/splashs/750x1334.png',
    '/img/splashs/828x1792.png',
    '/img/splashs/1125x2436.png',
    '/img/splashs/1242x2208.png',
    '/img/splashs/1242x2688.png',
    '/img/splashs/1536x2048.png',
    '/img/splashs/1668x2224.png',
    '/img/splashs/1668x2388.png',
    '/img/splashs/2048x2732.png',
    '/img/offline/offline.jpg',
    '/img/fondo/fondo.jpg',
    '/img/iconos/aulas.png',
    '/img/iconos/cuatrimestres.png',
    '/img/iconos/rfid.png'
];

let INMUTABLE = [
    '/css/app.css',
    '/js/app.js',
    '/fonts/vendor/bootstrap-icons/bootstrap-icons.woff',
    '/fonts/vendor/bootstrap-icons/bootstrap-icons.woff2',
    '/fonts/vendor/boxicons/boxicons.eot',
    '/fonts/vendor/boxicons/boxicons.svg',
    '/fonts/vendor/boxicons/boxicons.ttf',
    '/fonts/vendor/boxicons/boxicons.woff',
    '/fonts/vendor/boxicons/boxicons.woff2',
    '/vendor/aos/css/aos.css',
    '/vendor/aos/js/aos.js',
    '/vendor/glightbox/css/glightbox.css',
    '/vendor/glightbox/js/glightbox.js',
    '/vendor/glightbox/css/glightbox.min.css',
    '/vendor/glightbox/js/glightbox.min.js',
    '/vendor/glightbox/css/plyr.css',
    '/vendor/glightbox/css/plyr.min.css',
    '/vendor/isotope-layout/isotope.pkgd.js',
    '/vendor/isotope-layout/isotope.pkgd.min.js',
    '/vendor/livewire/livewire.js',
    '/vendor/livewire/livewire.js.map',
    '/vendor/livewire/manifest.json',
    '/vendor/php-email-form/validate.js',
    '/vendor/pouchdb/pouchdb.js',
    '/vendor/pouchdb/pouchdb.min.js',
    '/vendor/purecounter/purecounter_vanilla.js',
    '/vendor/swiper/css/swiper-bundle.min.css',
    '/vendor/swiper/js/swiper-bundle.min.js',
    '/vendor/telescope/app-dark.css',
    '/vendor/telescope/app.css',
    '/vendor/telescope/app.js',
    '/vendor/telescope/favicon.ico',
    '/vendor/telescope/mix-manifest.json',
    '/vendor/typed/typed.js',
    '/vendor/typed/typed.min.js',
    '/vendor/typed/typed.min.js.map',
    '/vendor/waypoints/noframework.waypoints.js'
];

//Clear cache Dynamic
function deleteCache(CACHE_NAME, NUM_ITEMS) {
    caches.open(CACHE_NAME).then(async cache => {
        const keys = await cache.keys();
        if (keys.length > NUM_ITEMS) {
            cache.delete(keys[0]).then(
                deleteCache(CACHE_NAME, NUM_ITEMS)
            );
        };
    });
};

// Cache on install
self.addEventListener("install", (event) => {
    this.skipWaiting();
    event.waitUntil(
        caches.open(CACHE_STATIC_NAME).then((cache) => {
            return cache.addAll(APP_SHELL);
        }),
        caches.open(CACHE_INMUTABLE_NAME).then(cache => {
            return cache.addAll(INMUTABLE);
        })
    );
});

// Clear cache on activate
self.addEventListener("activate", (event) => {
    event.waitUntil(
        caches.keys().then((keys) => {
            keys.forEach((key) => {
                if (key !== CACHE_STATIC_NAME && key.includes('pwa-static-app_shell-')) {
                    return caches.delete(key);
                };
                if (key !== CACHE_INMUTABLE_NAME && key.includes('pwa-inmutable-')) {
                    return caches.delete(key);
                };
                if (key !== CACHE_DYNAMIC_NAME && key.includes('pwa-dynamic-')) {
                    return caches.delete(key);
                };
            });
        }),
    );
});

// Serve from Cache With Network Fallback
self.addEventListener("fetch", (event) => {
    if (!(event.request.url.indexOf('http') == 0)) return;
    event.respondWith(
        caches.match(event.request).then((response) => {
            if (response) {
                return response;
            } else {
                return (
                    fetch(event.request).then((newResponse) => {
                        return caches.open(CACHE_DYNAMIC_NAME).then((cache) => {
                            if(newResponse.type == 'basic' || newResponse.type == 'cors' || newResponse.type == 'opaque'){
                                cache.put(event.request.url, newResponse.clone());
                                deleteCache(CACHE_DYNAMIC_NAME, CACHE_DYNAMIC_LIMIT);
                            };
                            return newResponse;
                        });
                    }).catch(() => {
                        return caches.match("offline");
                    })
                );
            }
        })
    );
});

// Push
self.addEventListener("push", (event) => {
    if (event.data) {
        let msg = event.data.json();
        event.waitUntil(self.registration.showNotification(msg.title, {
            body: msg.body,
            icon: msg.icon,
            vibrate: msg.vibrate
        }));
    }
});