/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/offline.js":
/*!*********************************!*\
  !*** ./resources/js/offline.js ***!
  \*********************************/
/***/ (() => {

eval("(function () {\n  \"use strict\";\n\n  var estadoRed = function estadoRed() {\n    window.location.reload();\n    if (navigator.onLine) {\n      Swal.fire({\n        position: 'top-end',\n        icon: 'success',\n        title: 'En línea',\n        showConfirmButton: false,\n        timer: 1500\n      });\n    } else {\n      Swal.fire({\n        position: 'top-end',\n        icon: 'warning',\n        title: 'Fuera de línea',\n        showConfirmButton: false,\n        timer: 1500\n      });\n    }\n    ;\n  };\n  window.addEventListener('offline', function () {\n    estadoRed();\n  });\n  window.addEventListener('online', function () {\n    estadoRed();\n  });\n})();//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJlc3RhZG9SZWQiLCJ3aW5kb3ciLCJsb2NhdGlvbiIsInJlbG9hZCIsIm5hdmlnYXRvciIsIm9uTGluZSIsIlN3YWwiLCJmaXJlIiwicG9zaXRpb24iLCJpY29uIiwidGl0bGUiLCJzaG93Q29uZmlybUJ1dHRvbiIsInRpbWVyIiwiYWRkRXZlbnRMaXN0ZW5lciJdLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvb2ZmbGluZS5qcz85OGNhIl0sInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigpIHtcclxuICAgIFwidXNlIHN0cmljdFwiO1xyXG5cclxuICAgIGNvbnN0IGVzdGFkb1JlZD0gKCgpID0+IHtcclxuICAgICAgICB3aW5kb3cubG9jYXRpb24ucmVsb2FkKCk7XHJcbiAgICAgICAgaWYgKG5hdmlnYXRvci5vbkxpbmUpIHtcclxuICAgICAgICAgICAgU3dhbC5maXJlKHtcclxuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiAndG9wLWVuZCcsXHJcbiAgICAgICAgICAgICAgICBpY29uOiAnc3VjY2VzcycsXHJcbiAgICAgICAgICAgICAgICB0aXRsZTogJ0VuIGzDrW5lYScsXHJcbiAgICAgICAgICAgICAgICBzaG93Q29uZmlybUJ1dHRvbjogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICB0aW1lcjogMTUwMFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBTd2FsLmZpcmUoe1xyXG4gICAgICAgICAgICAgICAgcG9zaXRpb246ICd0b3AtZW5kJyxcclxuICAgICAgICAgICAgICAgIGljb246ICd3YXJuaW5nJyxcclxuICAgICAgICAgICAgICAgIHRpdGxlOiAnRnVlcmEgZGUgbMOtbmVhJyxcclxuICAgICAgICAgICAgICAgIHNob3dDb25maXJtQnV0dG9uOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIHRpbWVyOiAxNTAwXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH07XHJcbiAgICB9KTtcclxuXHJcbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignb2ZmbGluZScsICgpID0+IHsgZXN0YWRvUmVkKCkgfSk7XHJcbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignb25saW5lJywgKCkgPT4geyBlc3RhZG9SZWQoKSB9KTtcclxuICB9KSgpIl0sIm1hcHBpbmdzIjoiQUFBQSxDQUFDLFlBQVc7RUFDUixZQUFZOztFQUVaLElBQU1BLFNBQVMsR0FBRyxTQUFaQSxTQUFTLEdBQVM7SUFDcEJDLE1BQU0sQ0FBQ0MsUUFBUSxDQUFDQyxNQUFNLEVBQUU7SUFDeEIsSUFBSUMsU0FBUyxDQUFDQyxNQUFNLEVBQUU7TUFDbEJDLElBQUksQ0FBQ0MsSUFBSSxDQUFDO1FBQ05DLFFBQVEsRUFBRSxTQUFTO1FBQ25CQyxJQUFJLEVBQUUsU0FBUztRQUNmQyxLQUFLLEVBQUUsVUFBVTtRQUNqQkMsaUJBQWlCLEVBQUUsS0FBSztRQUN4QkMsS0FBSyxFQUFFO01BQ1gsQ0FBQyxDQUFDO0lBQ04sQ0FBQyxNQUFNO01BQ0hOLElBQUksQ0FBQ0MsSUFBSSxDQUFDO1FBQ05DLFFBQVEsRUFBRSxTQUFTO1FBQ25CQyxJQUFJLEVBQUUsU0FBUztRQUNmQyxLQUFLLEVBQUUsZ0JBQWdCO1FBQ3ZCQyxpQkFBaUIsRUFBRSxLQUFLO1FBQ3hCQyxLQUFLLEVBQUU7TUFDWCxDQUFDLENBQUM7SUFDTjtJQUFDO0VBQ0wsQ0FBRTtFQUVGWCxNQUFNLENBQUNZLGdCQUFnQixDQUFDLFNBQVMsRUFBRSxZQUFNO0lBQUViLFNBQVMsRUFBRTtFQUFDLENBQUMsQ0FBQztFQUN6REMsTUFBTSxDQUFDWSxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsWUFBTTtJQUFFYixTQUFTLEVBQUU7RUFBQyxDQUFDLENBQUM7QUFDMUQsQ0FBQyxHQUFHIiwiZmlsZSI6Ii4vcmVzb3VyY2VzL2pzL29mZmxpbmUuanMuanMiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./resources/js/offline.js\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval-source-map devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./resources/js/offline.js"]();
/******/ 	
/******/ })()
;