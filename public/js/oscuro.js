/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/oscuro.js":
/*!********************************!*\
  !*** ./resources/js/oscuro.js ***!
  \********************************/
/***/ (() => {

eval("(function () {\n  \"use strict\";\n\n  var body = document.querySelector('body'),\n    modeSwitch = body.querySelector(\".toggle-switch\"),\n    modeText = body.querySelector(\".mode-text\"),\n    modeIMG = body.querySelector(\".mode-img\");\n  modeSwitch.addEventListener(\"click\", function () {\n    body.classList.toggle(\"dark\");\n    if (body.classList.contains(\"dark\")) {\n      modeText.innerText = \"Modo Luz\";\n      modeIMG.src = 'img/logo uteams modo oscuro.png';\n    } else {\n      modeText.innerText = \"Modo \\n Oscuro\";\n      modeIMG.src = 'img/logo uteams.png';\n    }\n    ;\n  });\n})();//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJib2R5IiwiZG9jdW1lbnQiLCJxdWVyeVNlbGVjdG9yIiwibW9kZVN3aXRjaCIsIm1vZGVUZXh0IiwibW9kZUlNRyIsImFkZEV2ZW50TGlzdGVuZXIiLCJjbGFzc0xpc3QiLCJ0b2dnbGUiLCJjb250YWlucyIsImlubmVyVGV4dCIsInNyYyJdLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvb3NjdXJvLmpzPzY0MDMiXSwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCkge1xyXG4gICAgXCJ1c2Ugc3RyaWN0XCI7XHJcblxyXG4gICAgY29uc3QgYm9keSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ2JvZHknKSxcclxuICAgICAgbW9kZVN3aXRjaCA9IGJvZHkucXVlcnlTZWxlY3RvcihcIi50b2dnbGUtc3dpdGNoXCIpLFxyXG4gICAgICBtb2RlVGV4dCA9IGJvZHkucXVlcnlTZWxlY3RvcihcIi5tb2RlLXRleHRcIiksXHJcbiAgICAgIG1vZGVJTUcgPSBib2R5LnF1ZXJ5U2VsZWN0b3IoXCIubW9kZS1pbWdcIik7XHJcblxyXG4gICAgbW9kZVN3aXRjaC5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiAsICgpID0+e1xyXG4gICAgICAgIGJvZHkuY2xhc3NMaXN0LnRvZ2dsZShcImRhcmtcIik7XHJcbiAgICAgICAgaWYoYm9keS5jbGFzc0xpc3QuY29udGFpbnMoXCJkYXJrXCIpKXtcclxuICAgICAgICAgICAgbW9kZVRleHQuaW5uZXJUZXh0ID0gXCJNb2RvIEx1elwiO1xyXG4gICAgICAgICAgICBtb2RlSU1HLnNyYyA9ICdpbWcvbG9nbyB1dGVhbXMgbW9kbyBvc2N1cm8ucG5nJztcclxuICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgbW9kZVRleHQuaW5uZXJUZXh0ID0gXCJNb2RvIFxcbiBPc2N1cm9cIjtcclxuICAgICAgICAgICAgbW9kZUlNRy5zcmMgPSAnaW1nL2xvZ28gdXRlYW1zLnBuZyc7XHJcbiAgICAgICAgfTtcclxuICAgIH0pO1xyXG4gIH0pKCkiXSwibWFwcGluZ3MiOiJBQUFBLENBQUMsWUFBVztFQUNSLFlBQVk7O0VBRVosSUFBTUEsSUFBSSxHQUFHQyxRQUFRLENBQUNDLGFBQWEsQ0FBQyxNQUFNLENBQUM7SUFDekNDLFVBQVUsR0FBR0gsSUFBSSxDQUFDRSxhQUFhLENBQUMsZ0JBQWdCLENBQUM7SUFDakRFLFFBQVEsR0FBR0osSUFBSSxDQUFDRSxhQUFhLENBQUMsWUFBWSxDQUFDO0lBQzNDRyxPQUFPLEdBQUdMLElBQUksQ0FBQ0UsYUFBYSxDQUFDLFdBQVcsQ0FBQztFQUUzQ0MsVUFBVSxDQUFDRyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUcsWUFBSztJQUN2Q04sSUFBSSxDQUFDTyxTQUFTLENBQUNDLE1BQU0sQ0FBQyxNQUFNLENBQUM7SUFDN0IsSUFBR1IsSUFBSSxDQUFDTyxTQUFTLENBQUNFLFFBQVEsQ0FBQyxNQUFNLENBQUMsRUFBQztNQUMvQkwsUUFBUSxDQUFDTSxTQUFTLEdBQUcsVUFBVTtNQUMvQkwsT0FBTyxDQUFDTSxHQUFHLEdBQUcsaUNBQWlDO0lBQ25ELENBQUMsTUFBSTtNQUNEUCxRQUFRLENBQUNNLFNBQVMsR0FBRyxnQkFBZ0I7TUFDckNMLE9BQU8sQ0FBQ00sR0FBRyxHQUFHLHFCQUFxQjtJQUN2QztJQUFDO0VBQ0wsQ0FBQyxDQUFDO0FBQ0osQ0FBQyxHQUFHIiwiZmlsZSI6Ii4vcmVzb3VyY2VzL2pzL29zY3Vyby5qcy5qcyIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./resources/js/oscuro.js\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval-source-map devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./resources/js/oscuro.js"]();
/******/ 	
/******/ })()
;