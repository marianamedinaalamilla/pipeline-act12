<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGruposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('grupo', 10)->unique();
            $table->foreignId('carrera_id');
            $table->foreignId('cuatrimestre_id');
            $table->foreignId('aula_id');
            $table->foreignId('turno_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grupos');
    }
}
