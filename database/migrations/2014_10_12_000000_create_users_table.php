<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('nombre', 30);
            $table->string('apellido_paterno', 15);
            $table->string('apellido_materno', 15);
            $table->foreignId('genero_id');
            $table->string('email', 31)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('username', 10)->unique();
            $table->string('password', 100);
            $table->string('foto_perfil');
            $table->foreignId('estatu_usuario_id');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
