<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstudianteAsistenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudiante_asistencias', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->foreignId('carrera_id');
            $table->foreignId('cuatrimestre_id');
            $table->foreignId('grupo_id');
            $table->foreignId('asignatura_id');
            $table->foreignId('estudiante_id');
            $table->date('fecha');
            $table->time('hora');
            $table->foreignId('estatu_asistencia_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudiante_asistencias');
    }
}
