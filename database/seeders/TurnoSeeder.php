<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Turno;

class TurnoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $turno = new Turno();
		$turno->turno = "Matutino";
		$turno->save();
		
		$turno = new Turno();
		$turno->turno = "Vespertino";
		$turno->save();
    }
}
