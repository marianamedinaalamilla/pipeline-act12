<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Dia;

class DiaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dia = new Dia();
		$dia->dia = "Lunes";
		$dia->save();

        $dia = new Dia();
		$dia->dia = "Martes";
		$dia->save();

        $dia = new Dia();
		$dia->dia = "Miercoles";
		$dia->save();

        $dia = new Dia();
		$dia->dia = "Jueves";
		$dia->save();

        $dia = new Dia();
		$dia->dia = "Viernes";
		$dia->save();
    }
}
