<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Grado;
use App\Models\Carrera;

class CarreraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $carrera = new Carrera();
		$carrera->grado_id = Grado::where('grado', 'Técnico Superior Universitario')->first()->id;
        $carrera->carrera = 'Tecnologías de la Información Área Desarrollo de Software Multiplataforma';
		$carrera->save();
        
        $carrera = new Carrera();
		$carrera->grado_id = Grado::where('grado', 'Técnico Superior Universitario')->first()->id;
        $carrera->carrera = 'Gastronomía';
		$carrera->save();

        $carrera = new Carrera();
		$carrera->grado_id = Grado::where('grado', 'Técnico Superior Universitario')->first()->id;
        $carrera->carrera = 'Desarrollo de Negocios Área Mercadotecnia';
		$carrera->save();

        $carrera = new Carrera();
		$carrera->grado_id = Grado::where('grado', 'Técnico Superior Universitario')->first()->id;
        $carrera->carrera = 'Mecatrónica Área Sistema de Manufactura Flexible';
		$carrera->save();

        $carrera = new Carrera();
		$carrera->grado_id = Grado::where('grado', 'Ingeniería')->first()->id;
        $carrera->carrera = 'Desarrollo y Gestión de Software';
		$carrera->save();

        $carrera = new Carrera();
		$carrera->grado_id = Grado::where('grado', 'Licenciatura')->first()->id;
        $carrera->carrera = 'Gastronomía';
		$carrera->save();

        $carrera = new Carrera();
		$carrera->grado_id = Grado::where('grado', 'Licenciatura')->first()->id;
        $carrera->carrera = 'Innovación de Negocios y Mercadotecnia';
		$carrera->save();

        $carrera = new Carrera();
		$carrera->grado_id = Grado::where('grado', 'Ingeniería')->first()->id;
        $carrera->carrera = 'Mecatrónica';
		$carrera->save();
    }
}
