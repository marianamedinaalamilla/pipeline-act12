<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create(['name' => 'Administrador']);
        $role = Role::create(['guard_name' => 'api', 'name' => 'Administrador']);
        $role = Role::create(['name' => 'Director de TI']);
        $role = Role::create(['name' => 'Director de Mecatrónica']);
        $role = Role::create(['name' => 'Director de Gastronomía']);
        $role = Role::create(['name' => 'Director de Desarrollo de Negocios']);
        $role = Role::create(['name' => 'Docente']);
        $role = Role::create(['guard_name' => 'api', 'name' => 'Docente']);
        $role = Role::create(['name' => 'Estudiante']);
        $role = Role::create(['guard_name' => 'api', 'name' => 'Estudiante']);
        $role = Role::create(['guard_name' => 'api', 'name' => 'Guardia']);
    }
}
