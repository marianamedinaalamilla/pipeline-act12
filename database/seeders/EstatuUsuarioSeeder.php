<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\EstatuUsuario;

class EstatuUsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modo = new EstatuUsuario();
		$modo->estatu = "Activo";
		$modo->save();
		
		$modo = new EstatuUsuario();
		$modo->estatu = "Inactivo";
		$modo->save();
    }
}
