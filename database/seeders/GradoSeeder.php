<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Grado;

class GradoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $grado = new Grado();
		$grado->grado = "Ingeniería";
        $grado->abreviatura = "Ing";
		$grado->save();

        $grado = new Grado();
		$grado->grado = "Licenciatura";
        $grado->abreviatura = "Lic";
		$grado->save();

        $grado = new Grado();
		$grado->grado = "Técnico Superior Universitario";
        $grado->abreviatura = "TSU";
		$grado->save();
    }
}
