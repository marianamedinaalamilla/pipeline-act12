<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
			RoleSeeder::class,
            GeneroSeeder::class,
            TurnoSeeder::class,
            GradoSeeder::class,
            DiaSeeder::class,
            CarreraSeeder::class,
            EstatuUsuarioSeeder::class,
            UserSeeder::class,
            EstatuSensorSeeder::class,
            EstatuAsistenciaSeeder::class,
		]);
    }
}
