const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .js('resources/js/uteams.js', 'public/js')
    .sass('resources/sass/uteams.scss', 'public/css')
    .sass('resources/sass/sesion.scss', 'public/css')
    .sass('resources/sass/menu.scss', 'public/css')
    .js('resources/js/menu.js', 'public/js')
    .js('resources/js/alertas.js', 'public/js')
    .js('resources/js/offline.js', 'public/js')
    .js('resources/js/bd.js', 'public/js')
    .js('resources/js/navigator.js', 'public/js')
    .js('resources/js/push.js', 'public/js')
    .js('resources/js/oscuro.js', 'public/js')
    .copy('resources/img', 'public/img')
    .copy('resources/vendor', 'public/vendor')
    .sourceMaps();
