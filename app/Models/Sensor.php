<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sensor extends Model
{	
    public $timestamps = true;

    protected $table = 'sensores';

    protected $primaryKey = 'id';

    protected $fillable = ['modelo','descripcion','foto'];

    public function sensorGrupos(){
        return $this->hasMany(SensorGrupo::class);
    }
}