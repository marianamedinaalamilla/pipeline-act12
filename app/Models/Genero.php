<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Genero extends Model
{
    public $timestamps = true;

    protected $table = 'generos';

    protected $primaryKey = 'id';

    protected $fillable = ['genero'];
}
