<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Carrera extends Model
{	
    public $timestamps = true;

    protected $table = 'carreras';

    protected $primaryKey = 'id';

    protected $fillable = ['grado_id','carrera'];
	
    public function grado(){
		  return $this->belongsTo(Grado::class);
    }

    public function carreraCuatrimestres(){
        return $this->hasMany(CarreraCuatrimestre::class);
    }

    public function asignaturas(){
        return $this->hasMany(Asignatura::class);
    }

    public function docentes(){
        return $this->hasMany(Docente::class);
    }

    public function grupos(){
        return $this->hasMany(Grupo::class);
    }

    public function estudiantes(){
        return $this->hasMany(Estudiante::class);
    }
}
