<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grado extends Model
{
    public $timestamps = true;

    protected $table = 'grados';

    protected $primaryKey = 'id';

    protected $fillable = ['grado', 'abreviatura'];
}
