<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstudianteGrupo extends Model
{	
    public $timestamps = true;

    protected $table = 'estudiante_grupos';

    protected $primaryKey = 'id';

    protected $fillable = ['estudiante_id','grupo_id'];
	
    public function estudiante(){
        return $this->belongsTo(Estudiante::class);
    }
    
    public function grupo(){
        return $this->belongsTo(Grupo::class);
    }
}
