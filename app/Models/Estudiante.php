<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{	
    public $timestamps = true;

    protected $table = 'estudiantes';

    protected $primaryKey = 'id';

    protected $fillable = ['user_id','carrera_id','identificacion'];
	
    public function user(){
        return $this->belongsTo(User::class);
    }
    
    public function carrera(){
        return $this->belongsTo(Carrera::class);
    }
    
    public function estudianteGrupos(){
        return $this->hasMany(EstudianteGrupo::class);
    }

    public function estudianteAsistencias(){
        return $this->hasMany(EstudianteAsistencia::class);
    }
}
