<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cuatrimestre extends Model
{	
    public $timestamps = true;

    protected $table = 'cuatrimestres';

    protected $primaryKey = 'id';

    protected $fillable = ['cuatrimestre','periodo_cuatrimestral_inicial','periodo_cuatrimestral_final'];

    public function carreraCuatrimestres(){
        return $this->hasMany(CarreraCuatrimestre::class);
    }

    public function asignaturas(){
        return $this->hasMany(Asignatura::class);
    }

    public function grupos(){
        return $this->hasMany(Grupo::class);
    }
}
