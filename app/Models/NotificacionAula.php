<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificacionAula extends Model
{	
    public $timestamps = true;

    protected $table = 'notificacion_aulas';

    protected $primaryKey = 'id';

    protected $fillable = ['grupo_id','fecha','hora_abrir','hora_cerrar'];
	
    public function grupo(){
        return $this->belongsTo(Grupo::class);
    }
}
