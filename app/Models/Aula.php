<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aula extends Model
{	
    public $timestamps = true;

    protected $table = 'aulas';

    protected $primaryKey = 'id';

    protected $fillable = ['aula','departamento'];
	
    public function grupos(){
        return $this->hasMany(Grupo::class);
    }
}
