<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GrupoAsignatura extends Model
{	
    public $timestamps = true;

    protected $table = 'grupo_asignaturas';

    protected $primaryKey = 'id';

    protected $fillable = ['grupo_id','asignatura_id', 'dia_id', 'hora_entrada','hora_salida'];
	
    public function grupo(){
        return $this->belongsTo(Grupo::class);
    }
    
    public function asignatura(){
        return $this->belongsTo(Asignatura::class);
    }

    public function dia(){
        return $this->belongsTo(Dia::class);
    }
}
