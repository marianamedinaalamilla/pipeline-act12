<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Carrera;
use App\Models\Grado;

class Carreras extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap', $listeners = ['store', 'update', 'destroy'];
    public $identificador, $buscador, $filtroGrado, $tituloModulo, $grado_id, $carrera, $boton = false;

    public function render()
    {
        $carreras = Carrera::orderBy('carrera', 'ASC')->paginate(5);
        if($this->filtroGrado != ""){
            $this->buscador = null;
            $carreras = Carrera::orderBy('carrera', 'ASC')->where('grado_id', $this->filtroGrado)->paginate(5);
        } else {
            $this->filtroGrado = null;
        };
        if($this->buscador){
            $this->filtroGrado = null;
            $carreras = Carrera::orderBy('carrera', 'ASC')
                                ->where('carrera', 'LIKE', '%'.$this->buscador.'%')
                                ->paginate(5);
        };
        $grados = Grado::orderBy('grado', 'ASC')->get();
        return view('livewire.carreras.view', compact('carreras', 'grados'));
    }

    public function mount(){
		$this->tituloModulo = 'Carrera';
	}
	
    public function cancel()
    {
        $this->resetInput();
        $this->boton = false;
    }
	
    private function resetInput()
    {		
        $this->identificador = null;
		$this->grado_id = null;
		$this->carrera = null;
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function store()
    {
        $this->validate([
			'grado_id' => 'required|integer',
            'carrera' => 'required|alpha_spaces|min:1|max:100'
        ]);

        $carrera = new Carrera();
        $carrera->grado_id = $this->grado_id;
		$carrera->carrera = $this->carrera;
		$carrera->save();

        $this->resetInput();
		$this->emit('modalCerrar');
        $this->emit('registroGuardado');
    }

    public function edit($id)
    {
        $carrera = Carrera::findOrFail($id);

        $this->identificador = $id; 
		$this->grado_id = $carrera->grado_id;
		$this->carrera = $carrera->carrera;		
    }

    public function update()
    {
        $this->validate([
			'grado_id' => 'required|integer',
            'carrera' => 'required|alpha_spaces|min:1|max:100'
        ]);

        if ($this->identificador) {
			$carrera = Carrera::find($this->identificador);
            $carrera->grado_id = $this->grado_id;
		    $carrera->carrera = $this->carrera;
		    $carrera->save();

            $this->resetInput();
            $this->emit('modalCerrar');
			$this->emit('registroActualizado');
        }
    }

    public function destroy($id)
    {
        $carrera = Carrera::find($id);
        $carrera->delete();
        $this->emit('registroEliminado');
    }
}
