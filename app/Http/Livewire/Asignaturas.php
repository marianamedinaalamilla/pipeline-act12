<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Asignatura;
use App\Models\Carrera;
use App\Models\Cuatrimestre;

class Asignaturas extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap', $listeners = ['store', 'update', 'destroy'];
    public $identificador, $buscador, $filtroCarrera, $filtroCuatrimestre, $tituloModulo, $asignatura, $selectCarrera, $cuatrimestre_id, $cuatrimestres2, $boton = false;

    public function render()
    {
		$asignaturas = Asignatura::orderBy('asignatura', 'ASC')->paginate(5);
        if($this->filtroCarrera != ""){
            $this->buscador = null;
            $asignaturas = Asignatura::orderBy('asignatura', 'ASC')->where('carrera_id', $this->filtroCarrera)->paginate(5);
        } else {
            $this->filtroCarrera = null;
        };
        if($this->filtroCuatrimestre != ""){
            $this->buscador = null;
            $asignaturas = Asignatura::orderBy('asignatura', 'ASC')->where('cuatrimestre_id', $this->filtroCuatrimestre)->paginate(5);
        } else {
            $this->filtroCuatrimestre = null;
        };
        if($this->filtroCarrera && $this->filtroCuatrimestre){
            $asignaturas = Asignatura::orderBy('asignatura', 'ASC')
                                        ->where('carrera_id', $this->filtroCarrera)
                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                        ->paginate(5);
        };
        if($this->buscador){
            $this->filtroCarrera = null;
            $this->filtroCuatrimestre = null;
            $asignaturas = Asignatura::orderBy('asignatura', 'ASC')->where('asignatura', 'LIKE', '%'.$this->buscador.'%')->paginate(5);
        };
        $carreras = Carrera::orderBy('carrera', 'ASC')->get();
        $cuatrimestres = Cuatrimestre::orderBy('cuatrimestre', 'ASC')->get();
        return view('livewire.asignaturas.view', compact('asignaturas', 'carreras', 'cuatrimestres'));
    }

    public function mount(){
		$this->tituloModulo = 'Asignatura';
	}
	
    public function cancel()
    {
        $this->resetInput();
        $this->boton = false;
    }
	
    private function resetInput()
    {		
        $this->identificador = null;
		$this->asignatura = null;
        $this->selectCarrera = null;
        $this->cuatrimestre_id = null;
        $this->cuatrimestres2 = null;
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function store()
    {
        $this->validate([
            'asignatura' => 'required|alpha_spaces|min:1|max:100|unique:asignaturas',
            'selectCarrera' => 'required|integer',
            'cuatrimestre_id' => 'required|integer'
        ]);

        $asignatura = new Asignatura();
        $asignatura->asignatura = $this->asignatura;
        $asignatura->carrera_id = $this->selectCarrera;
        $asignatura->cuatrimestre_id = $this->cuatrimestre_id;
		$asignatura->save();
        
        $this->resetInput();
		$this->emit('modalCerrar');
        $this->emit('registroGuardado');
    }

    public function edit($id)
    {
        $asignatura = Asignatura::findOrFail($id);

        $this->cuatrimestres2 = Carrera::findOrFail($asignatura->carrera_id)->carreraCuatrimestres;
        $this->identificador = $id; 
		$this->asignatura = $asignatura->asignatura;
        $this->selectCarrera = $asignatura->carrera_id;
        $this->cuatrimestre_id = $asignatura->cuatrimestre_id;
    }

    public function update()
    {
        $this->validate([
            'asignatura' => 'required|alpha_spaces|min:1|max:100|unique:asignaturas,asignatura,'.$this->identificador,
            'selectCarrera' => 'required|integer',
            'cuatrimestre_id' => 'required|integer'
        ]);

        if ($this->identificador) {
			$asignatura = Asignatura::find($this->identificador);
            $asignatura->asignatura = $this->asignatura;
            $asignatura->carrera_id = $this->selectCarrera;
            $asignatura->cuatrimestre_id = $this->cuatrimestre_id;
		    $asignatura->save();

            $this->resetInput();
            $this->emit('modalCerrar');
			$this->emit('registroActualizado');
        }
    }

    public function destroy($id)
    {
        $asignatura = Asignatura::find($id);
        $asignatura->delete();
        $this->emit('registroEliminado');
    }

    public function updatedselectCarrera($id)
    {
        $this->cuatrimestres2 = Carrera::find($id)->carreraCuatrimestres()->orderBy('carrera_id')->get();
    }
}