<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\NotificacionAula;
use App\Models\Grupo;

class NotificacionAulas extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap', $listeners = ['store', 'update', 'destroy'];
    public $identificador, $filtroGrupo, $filtroFecha, $filtroHora, $tituloModulo, $grupo_id, $fecha, $hora_abrir, $hora_cerrar, $boton = false;

    public function render()
    {
        $notificacionAulas = NotificacionAula::orderBy('grupo_id', 'ASC')->paginate(10);
        if($this->filtroGrupo != ""){
            $notificacionAulas = NotificacionAula::orderBy('grupo_id', 'ASC')->where('grupo_id', $this->filtroGrupo)->paginate(10);
        } else {
            $this->filtroGrupo = null;
        };
        if($this->filtroFecha != ""){
            $notificacionAulas = NotificacionAula::orderBy('grupo_id', 'ASC')
                                                    ->where('fecha', $this->filtroFecha)
                                                    ->paginate(10);
        } else {
            $this->filtroFecha = null;
        };
        if($this->filtroHora != ""){
            $notificacionAulas = NotificacionAula::orderBy('grupo_id', 'ASC')
                                                    ->orWhere('hora_abrir', $this->filtroHora)
                                                    ->orWhere('hora_cerrar', $this->filtroHora)
                                                    ->paginate(10);
        } else {
            $this->filtroHora = null;
        };
        if($this->filtroGrupo && $this->filtroFecha){
            $notificacionAulas = NotificacionAula::orderBy('grupo_id', 'ASC')
                                                    ->where('grupo_id', $this->filtroGrupo)
                                                    ->where('fecha', $this->filtroFecha)
                                                    ->paginate(10);
        } elseif ($this->filtroGrupo &&  $this->filtroHora){
            $notificacionAulas = NotificacionAula::orderBy('grupo_id', 'ASC')
                                                    ->where('grupo_id', $this->filtroGrupo)
                                                    ->where(function ($query) {
                                                        $query->orWhere('hora_abrir', $this->filtroHora)
                                                            ->orWhere('hora_cerrar', $this->filtroHora);
                                                        })
                                                    ->paginate(10);
        } elseif ($this->filtroFecha && $this->filtroHora){
            $notificacionAulas = NotificacionAula::orderBy('grupo_id', 'ASC')
                                                    ->where('fecha', $this->filtroFecha)
                                                    ->where(function ($query) {
                                                        $query->orWhere('hora_abrir', $this->filtroHora)
                                                            ->orWhere('hora_cerrar', $this->filtroHora);
                                                        })
                                                    ->paginate(10);
        };
        if($this->filtroGrupo && $this->filtroHora && $this->filtroHora){
            $notificacionAulas = NotificacionAula::orderBy('grupo_id', 'ASC')
                                                    ->where('grupo_id', $this->filtroGrupo)
                                                    ->where('fecha', $this->filtroFecha)
                                                    ->where(function ($query) {
                                                        $query->orWhere('hora_abrir', $this->filtroHora)
                                                            ->orWhere('hora_cerrar', $this->filtroHora);
                                                        })
                                                    ->paginate(10);
        };
        $grupos = Grupo::orderBy('grupo', 'ASC')->get();
        return view('livewire.notificacion-aulas.view', compact('notificacionAulas', 'grupos'));
    }

    public function mount(){
		$this->tituloModulo = 'Notificación de aulas';
	}
	
    public function cancel()
    {
        $this->resetInput();
        $this->boton = false;
    }
	
    private function resetInput()
    {		
        $this->identificador = null;
		$this->grupo_id = null;
        $this->fecha = null;
		$this->hora_abrir = null;
        $this->hora_cerrar = null;
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function store()
    {
        $this->validate([
			'grupo_id' => 'required|integer',
            'fecha' => 'required|date',
            'hora_abrir' => 'required|date_format:H:i|before:hora_cerrar',
            'hora_cerrar' => 'required|date_format:H:i|after:hora_abrir'
        ]);

        $notificacionAula = new NotificacionAula();
        $notificacionAula->grupo_id = $this->grupo_id;
        $notificacionAula->fecha = $this->fecha;
		$notificacionAula->hora_abrir = $this->hora_abrir;
        $notificacionAula->hora_cerrar = $this->hora_cerrar;
		$notificacionAula->save();

        $this->resetInput();
		$this->emit('modalCerrar');
        $this->emit('registroGuardado');
    }

    public function edit($id)
    {
        $notificacionAula = NotificacionAula::findOrFail($id);

        $this->identificador = $id; 
		$this->grupo_id = $notificacionAula->grupo_id;
        $this->fecha = $notificacionAula->fecha;
		$this->hora_abrir = $notificacionAula->hora_abrir;
        $this->hora_cerrar = $notificacionAula->hora_cerrar;			
    }

    public function update()
    {
        $this->validate([
			'grupo_id' => 'required|integer',
            'fecha' => 'required|date',
            'hora_abrir' => 'required|time|before:hora_cerrar',
            'hora_cerrar' => 'required|time|after:hora_abrir'
        ]);

        if ($this->identificador) {
			$notificacionAula = NotificacionAula::find($this->identificador);
            $notificacionAula->grupo_id = $this->grupo_id;
            $notificacionAula->fecha = $this->fecha;
		    $notificacionAula->hora_abrir = $this->hora_abrir;
            $notificacionAula->hora_cerrar = $this->hora_cerrar;
		    $notificacionAula->save();

            $this->resetInput();
            $this->emit('modalCerrar');
			$this->emit('registroActualizado');
        }
    }

    public function destroy($id)
    {
        $notificacionAula = NotificacionAula::find($id);
        $notificacionAula->delete();
        $this->emit('registroEliminado');
    }
}
