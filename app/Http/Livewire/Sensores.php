<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;
use App\Models\Sensor;

class Sensores extends Component
{
    use WithPagination, WithFileUploads;

	protected $paginationTheme = 'bootstrap', $listeners = ['store', 'update', 'destroy', 'image'];
    public $identificador, $buscador, $tituloModulo, $modelo, $descripcion, $foto, $tmp, $boton = false;

    public function render()
    {
        $sensores = Sensor::orderBy('modelo', 'ASC')->paginate(5);
        if($this->buscador){
            $sensores = Sensor::orderBy('modelo', 'ASC')
                                ->where('modelo', 'LIKE', '%'.$this->buscador.'%')
                                ->paginate(5);
        };
        return view('livewire.sensores.view', compact('sensores'));
    }

	public function mount(){
		$this->tituloModulo = 'Sensor';
	}
	
    public function cancel()
    {
        $this->resetInput();
        $this->tmp = null;
        $this->boton = false;
    }
	
    private function resetInput()
    {	
        $this->identificador = null;	
        $this->modelo = null;
		$this->descripcion = null;
		$this->foto = null;
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function store()
    {
        $this->validate([
			'modelo' => 'required|min:1|max:15|unique:sensores',
			'descripcion' => 'required|alpha_spaces|min:1|max:100',
			'foto' => 'required|image|mimes:jpeg,png,jpg',
        ]);

		$sensor = new Sensor();
        $sensor->modelo = $this->modelo;
		$sensor->descripcion = $this->descripcion;
        $sensor->foto = 'storage/'.$this->foto->storeAs('img/sensor/'.$this->modelo, $this->modelo.'.'.$this->foto->extension());
		$sensor->save();
        
        $this->resetInput();
		$this->emit('modalCerrar');
        $this->emit('registroGuardado');
    }

    public function edit($id)
    {
        $sensor = Sensor::findOrFail($id);

        $this->identificador = $id; 
		$this->modelo = $sensor->modelo;
		$this->descripcion = $sensor->descripcion;
		$this->foto = $sensor->foto;
        $this->tmp = $sensor->foto;
        $this->boton = true;
    }

    public function update()
    {
        $this->validate([
			'modelo' => 'required|min:1|max:15|unique:sensores,modelo,'.$this->identificador,
			'descripcion' => 'required|alpha_spaces|min:1|max:100',
			'foto' => 'required|image|mimes:jpeg,png,jpg',
        ]);

        if ($this->identificador) {
			$sensor = Sensor::find($this->identificador);
            $sensor->modelo = $this->modelo;
            $sensor->descripcion = $this->descripcion;
            $sensor->save();

            $this->resetInput();
            $this->boton = false;
            $this->emit('modalCerrar');
			$this->emit('registroActualizado');
        };
    }

    public function destroy($id)
    {
		$sensor = Sensor::find($id);
        $url = $sensor->foto;
        if(substr_count($url, 'storage/')){
            $url = str_replace('storage/', '', $url);
            if(Storage::disk('public')->exists($url)){
                Storage::disk('public')->delete($url);
            };
        };
        $sensor->delete();
        $this->emit('registroEliminado');
    }

    public function image()
    {
        if ($this->identificador) {
            $sensor = Sensor::find($this->identificador);
            if($this->foto){
                $this->validate(['foto' => 'required|image|mimes:jpeg,png,jpg|dimensions:min_width=110,min_height=110,max_width=180,max_height=180']);
                Storage::disk('public')->delete($sensor->foto);
                $sensor->foto = 'storage/'.$this->fp->storeAs('img/sensor/'.$this->modelo, $this->modelo.'.'.$this->foto->extension());
                $sensor->save();
                
                $this->resetInput();
                $this->tmp = null;
                $this->boton = false;
                $this->emit('modalCerrar');
                $this->emit('imagenActualizado');
            };
        };
    }
}