<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\DocenteAsignatura;
use App\Models\Docente;
use App\Models\Cuatrimestre;
use App\Models\Asignatura;
use App\Models\Grupo;
use App\Models\Dia;

class DocenteAsignaturas extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap', $listeners = ['store', 'update', 'destroy'];
    public $identificador, $filtroDocente, $filtroCuatrimestre, $filtroAsignatura, $filtroGrupo, $filtroDia, $filtroHora, $tituloModulo, $selectDocente, $selectCuatrimestre, $asignatura_id, $grupo_id, $dia_id, $hora_entrada, $hora_salida, $cuatrimestres2, $carrera, $asignaturas2, $grupos2, $boton = false;

    public function render()
    {
        $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')->paginate(5);
        if($this->filtroDocente != ""){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')->where('docente_id', $this->filtroDocente)->paginate(5);
        } else {
            $this->filtroDocente = null;
        };
        if($this->filtroCuatrimestre != ""){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')->where('cuatrimestre_id', $this->filtroCuatrimestre)->paginate(5);
        } else {
            $this->filtroCuatrimestre = null;
        };
        if($this->filtroAsignatura != ""){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')->where('asignatura_id', $this->filtroAsignatura)->paginate(5);
        } else {
            $this->filtroAsignatura = null;
        };
        if($this->filtroGrupo != ""){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')->where('grupo_id', $this->filtroGrupo)->paginate(5);
        } else {
            $this->filtroGrupo = null;
        };
        if($this->filtroDia != ""){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')->where('dia_id', $this->filtroDia)->paginate(5);
        } else {
            $this->filtroDia = null;
        };
        if($this->filtroHora != ""){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->orWhere('hora_entrada', $this->filtroHora)
                                                        ->orWhere('hora_salida', $this->filtroHora)
                                                        ->paginate(10);
        } else {
            $this->filtroHora = null;
        };
        if($this->filtroDocente && $this->filtroCuatrimestre){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->paginate(5);

        } elseif ($this->filtroDocente && $this->filtroAsignatura){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->paginate(5);
        } elseif ($this->filtroDocente && $this->filtroGrupo){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->paginate(5);
        } elseif ($this->filtroDocente && $this->filtroDia){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where('dia_id', $this->filtroDia)
                                                        ->paginate(5);
        } elseif ($this->filtroDocente && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
            
        } elseif ($this->filtroCuatrimestre && $this->filtroAsignatura){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->paginate(5);
        } elseif ($this->filtroCuatrimestre && $this->filtroGrupo){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->paginate(5);
        } elseif ($this->filtroCuatrimestre && $this->filtroDia){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->where('dia_id', $this->filtroDia)
                                                        ->paginate(5);
        } elseif ($this->filtroCuatrimestre && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
        } elseif ($this->filtroAsignatura && $this->filtroGrupo){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->paginate(5);
        } elseif ($this->filtroAsignatura && $this->filtroDia){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where('dia_id', $this->filtroDia)
                                                        ->paginate(5);
        } elseif ($this->filtroAsignatura && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
        } elseif ($this->filtroGrupo && $this->filtroDia){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->where('dia_id', $this->filtroDia)
                                                        ->paginate(5);
        } elseif ($this->filtroGrupo && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
        } elseif ($this->filtroDia && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('dia_id', $this->filtroDia)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
        };
        if($this->filtroDocente && $this->filtroCuatrimestre && $this->filtroAsignatura){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->paginate(5);
        } elseif ($this->filtroDocente && $this->filtroCuatrimestre && $this->filtroGrupo){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->paginate(5);
        } elseif ($this->filtroDocente && $this->filtroCuatrimestre && $this->filtroDia){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->where('dia_id', $this->filtroDia)
                                                        ->paginate(5);
        } elseif ($this->filtroDocente && $this->filtroCuatrimestre && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
        } elseif ($this->filtroDocente && $this->filtroAsignatura && $this->filtroGrupo){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->paginate(5);
        } elseif ($this->filtroDocente && $this->filtroAsignatura && $this->filtroDia){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where('dia_id', $this->filtroDia)
                                                        ->paginate(5);
        } elseif ($this->filtroDocente && $this->filtroAsignatura && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
        } elseif ($this->filtroDocente && $this->filtroGrupo && $this->filtroDia){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->where('dia_id', $this->filtroDia)
                                                        ->paginate(5);
        } elseif ($this->filtroDocente && $this->filtroGrupo && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
        } elseif ($this->filtroDocente && $this->filtroDia && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where('dia_id', $this->filtroDia)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
        } elseif ($this->filtroCuatrimestre && $this->filtroAsignatura && $this->filtroGrupo){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->paginate(5);
        } elseif ($this->filtroCuatrimestre && $this->filtroAsignatura && $this->filtroDia){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where('dia_id', $this->filtroDia)
                                                        ->paginate(5);
        } elseif ($this->filtroCuatrimestre && $this->filtroAsignatura && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
        } elseif ($this->filtroCuatrimestre && $this->filtroGrupo && $this->filtroDia){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->where('dia_id', $this->filtroDia)
                                                        ->paginate(5);
        } elseif ($this->filtroCuatrimestre && $this->filtroGrupo && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
        } elseif ($this->filtroCuatrimestre && $this->filtroDia && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->where('dia_id', $this->filtroDia)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
        } elseif ($this->filtroAsignatura && $this->filtroGrupo && $this->filtroDia){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->where('dia_id', $this->filtroDia)
                                                        ->paginate(5);
        } elseif ($this->filtroAsignatura && $this->filtroGrupo && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
        } elseif ($this->filtroAsignatura && $this->filtroDia && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where('dia_id', $this->filtroDia)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
        } elseif ($this->filtroGrupo && $this->filtroDia && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->where('dia_id', $this->filtroDia)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
        };
        if($this->filtroDocente && $this->filtroCuatrimestre && $this->filtroAsignatura && $this->filtroGrupo){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->paginate(5);
        } elseif ($this->filtroDocente && $this->filtroCuatrimestre && $this->filtroAsignatura && $this->filtroDia){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where('dia_dia', $this->filtroDia)
                                                        ->paginate(5);
        } elseif ($this->filtroDocente && $this->filtroCuatrimestre && $this->filtroAsignatura && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
        } elseif ($this->filtroDocente && $this->filtroAsignatura && $this->filtroGrupo && $this->filtroDia){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->where('dia_dia', $this->filtroDia)
                                                        ->paginate(5);
        } elseif ($this->filtroDocente && $this->filtroAsignatura && $this->filtroGrupo && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
        } elseif ($this->filtroDocente && $this->filtroGrupo && $this->filtroDia && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->where('dia_id', $this->filtroDia)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
        } elseif ($this->filtroCuatrimestre && $this->filtroAsignatura && $this->filtroGrupo && $this->filtroDia){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->where('dia_dia', $this->filtroDia)
                                                        ->paginate(5);
        } elseif ($this->filtroCuatrimestre && $this->filtroAsignatura && $this->filtroGrupo && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
        } elseif ($this->filtroCuatrimestre && $this->filtroGrupo && $this->filtroDia && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->where('dia_id', $this->filtroDia)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
        } elseif ($this->filtroAsignatura && $this->filtroGrupo && $this->filtroDia && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->where('dia_id', $this->filtroDia)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
        };
        if($this->filtroDocente && $this->filtroCuatrimestre && $this->filtroAsignatura && $this->filtroGrupo && $this->filtroDia){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->where('dia_id', $this->filtroDia)
                                                        ->paginate(5);
        } elseif ($this->filtroDocente && $this->filtroCuatrimestre && $this->filtroAsignatura && $this->filtroGrupo && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
        } elseif ($this->filtroDocente && $this->filtroAsignatura && $this->filtroGrupo && $this->filtroDia && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->where('dia_id', $this->filtroDia)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
        } elseif ($this->filtroCuatrimestre && $this->filtroAsignatura && $this->filtroGrupo && $this->filtroDia && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->where('dia_id', $this->filtroDia)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
        };
        if($this->filtroDocente && $this->filtroCuatrimestre && $this->filtroAsignatura && $this->filtroGrupo && $this->filtroDia && $this->filtroHora){
            $docenteAsignaturas = DocenteAsignatura::orderBy('docente_id', 'ASC')
                                                        ->where('docente_id', $this->filtroDocente)
                                                        ->where('cuatrimestre_id', $this->filtroCuatrimestre)
                                                        ->where('asignatura_id', $this->filtroAsignatura)
                                                        ->where('grupo_id', $this->filtroGrupo)
                                                        ->where('dia_id', $this->filtroDia)
                                                        ->where(function ($query) {
                                                            $query->orWhere('hora_entrada', $this->filtroHora)
                                                                ->orWhere('hora_salida', $this->filtroHora);
                                                            })
                                                        ->paginate(5);
        };
        $docentes = Docente::orderBy('user_id', 'DESC')->get();
        $cuatrimestres = Cuatrimestre::orderBy('cuatrimestre', 'ASC')->get();
        $asignaturas = Asignatura::orderBy('asignatura', 'ASC')->get();
        $grupos = Grupo::orderBy('grupo', 'ASC')->get();
        $dias = Dia::all();
        return view('livewire.docente-asignaturas.view', compact('docenteAsignaturas', 'docentes', 'cuatrimestres', 'asignaturas', 'grupos', 'dias'));
    }

    public function mount(){
		$this->tituloModulo = 'Asignatura';
	}
	
    public function cancel()
    {
        $this->resetInput();
        $this->boton = false;
    }
	
    private function resetInput()
    {		
        $this->identificador = null;
		$this->selectDocente = null;
		$this->selectCuatrimestre = null;
        $this->asignatura_id = null;
        $this->grupo_id = null;
        $this->dia_id = null;
        $this->hora_entrada = null;
        $this->hora_salida = null;
        $this->cuatrimestres2 = null;
        $this->carrera = null;
        $this->asignaturas2 = null;
        $this->grupos2 = null;
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function store()
    {
        $this->validate([
			'selectDocente' => 'required|integer',
            'selectCuatrimestre' => 'required|integer',
            'asignatura_id' => 'required|integer',
            'grupo_id' => 'required|integer',
            'dia_id' => 'required|integer',
            'hora_entrada' => 'required|date_format:H:i|before:hora_salida',
            'hora_salida' => 'required|date_format:H:i|after:hora_entrada'
        ]);

        $docenteAsignatura = new DocenteAsignatura();
        $docenteAsignatura->docente_id = $this->selectDocente;
		$docenteAsignatura->cuatrimestre_id = $this->selectCuatrimestre;
        $docenteAsignatura->asignatura_id = $this->asignatura_id;
        $docenteAsignatura->grupo_id = $this->grupo_id;
        $docenteAsignatura->dia_id = $this->dia_id;
        $docenteAsignatura->hora_entrada = $this->hora_entrada;
        $docenteAsignatura->hora_salida = $this->hora_salida;
		$docenteAsignatura->save();

        $this->resetInput();
		$this->emit('modalCerrar');
        $this->emit('registroGuardado');
    }

    public function edit($id)
    {
        $docenteAsignatura = DocenteAsignatura::findOrFail($id);
        $docente = Docente::findOrFail($docenteAsignatura->docente_id);
        $cuatrimestre = Cuatrimestre::findOrFail($docenteAsignatura->cuatrimestre_id);

        $this->cuatrimestres2 = $docente->carrera->carreraCuatrimestres()->orderBy('carrera_id')->get();
        $this->carrera = $docente->carrera_id;
        $this->asignaturas2 = $cuatrimestre->asignaturas()->orderBy('asignatura')->where('carrera_id',  $this->carrera)->get();
        $this->grupos2 = $cuatrimestre->grupos()->orderBy('grupo')->where('carrera_id',  $this->carrera)->get();
        $this->identificador = $id; 
		$this->selectDocente = $docenteAsignatura->docente_id;
		$this->selectCuatrimestre = $docenteAsignatura->cuatrimestre_id;
        $this->asignatura_id = $docenteAsignatura->asignatura_id;
        $this->grupo_id = $docenteAsignatura->grupo_id;
        $this->dia_id = $docenteAsignatura->dia_id;
        $this->hora_entrada = $docenteAsignatura->hora_entrada;
        $this->hora_salida = $docenteAsignatura->hora_salida;		
    }

    public function update()
    {
        $this->validate([
			'selectDocente' => 'required|integer',
            'selectCuatrimestre' => 'required|integer',
            'asignatura_id' => 'required|integer',
            'grupo_id' => 'required|integer',
            'dia_id' => 'required|integer',
            'hora_entrada' => 'required|date_format:H:i|before:hora_salida',
            'hora_salida' => 'required|date_format:H:i|after:hora_entrada'
        ]);

        if ($this->identificador) {
			$docenteAsignatura = DocenteAsignatura::find($this->identificador);
            $docenteAsignatura->docente_id = $this->selectDocente;
            $docenteAsignatura->cuatrimestre_id = $this->selectCuatrimestre;
            $docenteAsignatura->asignatura_id = $this->asignatura_id;
            $docenteAsignatura->grupo_id = $this->grupo_id;
            $docenteAsignatura->dia_id = $this->dia_id;
            $docenteAsignatura->hora_entrada = $this->hora_entrada;
            $docenteAsignatura->hora_salida = $this->hora_salida;
            $docenteAsignatura->save();

            $this->resetInput();
            $this->emit('modalCerrar');
			$this->emit('registroActualizado');
        }
    }

    public function destroy($id)
    {
        $docenteAsignatura = DocenteAsignatura::find($id);
        $docenteAsignatura->delete();
        $this->emit('registroEliminado');
    }

    public function updatedselectDocente($id)
    {
        $docente = Docente::find($id);
        $this->carrera = $docente->carrera_id;
        $this->cuatrimestres2 = $docente->carrera->carreraCuatrimestres()->orderBy('carrera_id')->get();
    }

    public function updatedselectCuatrimestre($id)
    {
        $cuatrimestre = Cuatrimestre::find($id);
        $this->asignaturas2 = $cuatrimestre->asignaturas()->orderBy('asignatura')->where('carrera_id',  $this->carrera)->get();
        $this->grupos2 = $cuatrimestre->grupos()->orderBy('grupo')->where('carrera_id',  $this->carrera)->get();
    }
}
