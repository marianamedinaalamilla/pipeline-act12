<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Aula;

class Aulas extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap', $listeners = ['store', 'update', 'destroy'];
    public $identificador, $filtroAula, $buscador, $tituloModulo, $aula, $departamento, $boton = false;

    public function render()
    {
        $aulas = Aula::orderBy('aula', 'ASC')->paginate(5);
        if($this->filtroAula != ""){
            $this->buscador = null;
            $aulas = Aula::orderBy('aula', 'ASC')->where('aula', $this->filtroAula)->paginate(5);
        } else {
            $this->filtroAula = null;
        };
        if($this->buscador){
            $this->filtroAula = null;
            $aulas = Aula::orderBy('aula', 'ASC')
                                ->where('departamento', 'LIKE', '%'.$this->buscador.'%')
                                ->paginate(5);
        };
        $aulas2 = Aula::orderBy('aula', 'ASC')->paginate(10);
        return view('livewire.aulas.view', compact('aulas', 'aulas2'));
    }

    public function mount(){
		$this->tituloModulo = 'Aula';
	}
	
    public function cancel()
    {
        $this->resetInput();
        $this->boton = false;
    }
	
    private function resetInput()
    {		
        $this->identificador = null;
		$this->aula = null;
		$this->departamento = null;
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function store()
    {
        $this->validate([
			'aula' => 'required|integer|min:1|max:15|unique:aulas',
            'departamento' => 'required|alpha_spaces|min:1'
        ]);

        $aula = new Aula();
        $aula->aula = $this->aula;
		$aula->departamento = $this->departamento;
		$aula->save();

        $this->resetInput();
		$this->emit('modalCerrar');
        $this->emit('registroGuardado');
    }

    public function edit($id)
    {
        $aula = Aula::findOrFail($id);

        $this->identificador = $id; 
		$this->aula = $aula->aula;
		$this->departamento = $aula->departamento;		
    }

    public function update()
    {
        $this->validate([
			'aula' => 'required|integer|min:1|max:15|unique:aulas,aula,'.$this->identificador,
            'departamento' => 'required|alpha_spaces|min:1'
        ]);

        if ($this->identificador) {
			$aula = Aula::find($this->identificador);
            $aula->aula = $this->aula;
		    $aula->departamento = $this->departamento;
		    $aula->save();

            $this->resetInput();
            $this->emit('modalCerrar');
			$this->emit('registroActualizado');
        }
    }

    public function destroy($id)
    {
        $aula = Aula::find($id);
        $aula->delete();
        $this->emit('registroEliminado');
    }
}