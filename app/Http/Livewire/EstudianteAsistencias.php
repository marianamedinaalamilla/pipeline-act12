<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\EstudianteAsistencia;

class EstudianteAsistencias extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';

    public function render()
    {
		$estudianteAsistencias = EstudianteAsistencia::paginate(5);
        
        return view('livewire.estudiante-asistencias.view', compact('estudianteAsistencias'));
    }
}