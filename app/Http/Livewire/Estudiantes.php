<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Estudiante;
use App\Models\User;
use App\Models\Carrera;

class Estudiantes extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap', $listeners = ['store', 'update', 'destroy'];
    public $identificador, $buscador, $filtroUser, $filtroCarrera, $tituloModulo, $user_id, $carrera_id, $identificacion, $boton = false;

    public function render()
    {
        $estudiantes = Estudiante::orderBy('user_id', 'DESC')->paginate(5);
        if($this->filtroUser != ""){
            $this->buscador = null;
            $estudiantes = Estudiante::orderBy('user_id', 'DESC')->where('user_id', $this->filtroUser)->paginate(5);
        } else {
            $this->filtroUser = null;
        };
        if($this->filtroCarrera != ""){
            $this->buscador = null;
            $estudiantes = Estudiante::orderBy('user_id', 'DESC')->where('carrera_id', $this->filtroCarrera)->paginate(5);
        } else {
            $this->filtroCarrera = null;
        };
        if($this->filtroUser && $this->filtroCarrera){
            $estudiantes = Estudiante::orderBy('user_id', 'DESC')
                                    ->where('user_id', $this->filtroUser)
                                    ->where('carrera_id', $this->filtroCarrera)
                                    ->paginate(5);
        };
        if($this->buscador){
            $this->filtroUser = null;
            $this->filtroCarrera = null;
            $estudiantes = Estudiante::orderBy('user_id', 'DESC')
                                    ->where('identificacion', 'LIKE', '%'.$this->buscador.'%')
                                    ->paginate(5);
        };
        $users = User::orderBy('apellido_paterno', 'ASC')->get();
        $carreras = Carrera::orderBy('carrera', 'ASC')->get();
        return view('livewire.estudiantes.view', compact('estudiantes', 'users', 'carreras'));
    }

    public function mount(){
		$this->tituloModulo = 'Estudiante';
	}
	
    public function cancel()
    {
        $this->resetInput();
        $this->boton = false;
    }
	
    private function resetInput()
    {		
        $this->identificador = null;
		$this->user_id = null;
		$this->carrera_id = null;
        $this->identificacion = null;
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function store()
    {
        $this->validate([
			'user_id' => 'required|integer',
            'carrera_id' => 'required|integer',
            'identificacion' => 'required|alpha_num|min:1|max:100|unique:estudiantes',
        ]);

        $estudiante = new Estudiante();
        $estudiante->user_id = $this->user_id;
		$estudiante->carrera_id = $this->carrera_id;
        $estudiante->identificacion = $this->identificacion;
		$estudiante->save();

        $this->resetInput();
		$this->emit('modalCerrar');
        $this->emit('registroGuardado');
    }

    public function edit($id)
    {
        $estudiante = Estudiante::findOrFail($id);

        $this->identificador = $id; 
		$this->user_id = $estudiante->user_id;
		$this->carrera_id = $estudiante->carrera_id;		
        $this->identificacion = $estudiante->identificacion;	
    }

    public function update()
    {
        $this->validate([
			'user_id' => 'required|integer',
            'carrera_id' => 'required|integer',
            'identificacion' => 'required|alpha_num|min:1|max:100|unique:estudiantes,identificacion,'.$this->identificador
        ]);

        if ($this->identificador) {
            $estudiante = Estudiante::find($this->identificador);
			$estudiante->user_id = $this->user_id;
            $estudiante->carrera_id = $this->carrera_id;
            $estudiante->identificacion = $this->identificacion;
            $estudiante->save();

            $this->resetInput();
            $this->emit('modalCerrar');
			$this->emit('registroActualizado');
        }
    }

    public function destroy($id)
    {
        $estudiante = Estudiante::find($id);
        $estudiante->delete();
        $this->emit('registroEliminado');
    }
}
