<?php

namespace App\Http\Controllers\APIs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\NotificacionAula;
use App\Models\Grupo;

class NotificacionAulaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notificacionAula = NotificacionAula::with('grupo')->orderBy('grupo_id', 'ASC')->get();
        return response()->json([ 'notificaciones' => $notificacionAula ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'grupo' => 'required|alpha_num|min:1|max:10',
			'ha' => 'required|time|before:hc',
			'hc' => 'required|time|after:ha'
		]);

        $notificacionAula = new User();
        $notificacionAula->grupo_id = Grupo::where('grupo', $request->grupo)->first()->id;
		$notificacionAula->hora_abrir = $request->ha;
        $notificacionAula->hora_cerrar = $request->hc;
		$notificacionAula->save();
        return response()->json([   
            'mensaje' => 'La notificación del grupo '.$notificacionAula->grupo->grupo.' se guardó con éxito.',
            'notificacionAula' => $notificacionAula->load(['grupo'])
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notificacionAula = NotificacionAula::with('grupo')->find($id);
        return response()->json([ 'notificación' => $notificacionAula ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
