(function() {
    "use strict";

    const estadoRed= (() => {
        window.location.reload();
        if (navigator.onLine) {
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'En línea',
                showConfirmButton: false,
                timer: 1500
            });
        } else {
            Swal.fire({
                position: 'top-end',
                icon: 'warning',
                title: 'Fuera de línea',
                showConfirmButton: false,
                timer: 1500
            });
        };
    });

    window.addEventListener('offline', () => { estadoRed() });
    window.addEventListener('online', () => { estadoRed() });
  })()