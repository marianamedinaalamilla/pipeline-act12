(function() {
  "use strict";
  
  navigator.serviceWorker.ready.then(async registration => {
    if (registration.sync) {
      try {
        await registration.sync.register("uteams");
      } catch {
        console.error("¡No se pudo registrar la sincronización!");
      }
    } else {
        console.error("Este navegador no es compatible con la sincronización");
    };
  });

  const db = new PouchDB('cuatrimestre');

  if (!navigator.onLine) {
    let pathname = window.location.pathname;
    
    if(pathname == "/cuatrimestres"){
      Livewire.on('agregarRegistro', () => {
        let cuatrimestre = $('.cuatrimestre').val();
        let pci = $('.pci').val();
        let pcf = $('.pcf').val();
        let documento = { _id: new Date().toISOString(), cuatrimestre, pci, pcf};
        db.put(documento).then(() => {
          Livewire.emit('modalCerrar');
          Livewire.emit('registroGuardado');
        });
      });
    };
  } else {
    Livewire.on('agregarRegistro', () => {
      Livewire.emitTo('users', 'store');
      Livewire.emitTo('cuatrimestres', 'store');
      Livewire.emitTo('carreras', 'store');
      Livewire.emitTo('carrera-cuatrimestres', 'store');
      Livewire.emitTo('aulas', 'store');
      Livewire.emitTo('asignaturas', 'store');
      Livewire.emitTo('plan-estudios', 'store');
      Livewire.emitTo('grupos', 'store');
      Livewire.emitTo('grupo-asignaturas', 'store');
      Livewire.emitTo('sensores', 'store');
      Livewire.emitTo('sensor-grupos', 'store');
      Livewire.emitTo('docentes', 'store');
      Livewire.emitTo('docente-asignaturas', 'store');
      Livewire.emitTo('estudiantes', 'store');
      Livewire.emitTo('estudiante-grupos', 'store'); 
      Livewire.emitTo('asistencias', 'store');
      Livewire.emitTo('notificaciones', 'store');
    });

    navigator.serviceWorker.ready.then((registration) => {
      if (registration.sync) {
        registration.sync.getTags().then((tags) => {
          if (tags.includes("uteams")){
            db.allDocs({ include_docs: true }).then( docs => {
              docs.rows.forEach((row) => {
                let token = document.querySelector('meta[name=csrf-token]').getAttribute('content');
                let documento = row.doc;
                fetch('/cuatrimestres/pouchdb', {
                  method: 'POST',
                  body: JSON.stringify(documento),
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'X-CSRF-Token': token
                  }
                }).then(() => {
                  return db.remove(documento);
                });            
              }); 
            });
          }
        });
      };
    });
  };
})()