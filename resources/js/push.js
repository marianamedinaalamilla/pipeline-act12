(function() {
  "use strict";
  
  //Get pathname
  let pathname = window.location.pathname;

  //Initialize PushManager
  if(!"PushManager" in window){
    //The Push Manager isn't supported on this browser
    console.error('Este navegador no es compatible con las notificaciones push.');
  };

  //Initialize notification
  if ("Notification" in window) {
    //Notification supported on this browser
    if (Notification.permission !== 'denied' || Notification.permission === "default") {
        // We need to ask the user for permission
        Notification.requestPermission().then((permission) => {
            // If the user accepts, let's create a notification
            if (permission == "granted") {
                // Use serviceWorker.ready to ensure that you can subscribe for push
                navigator.serviceWorker.ready.then(async(registration) => {
                  const options = { userVisibleOnly: true, applicationServerKey: urlBase64ToUint8Array('BB8DgvrmdNIbYSwDo9gbdAgKJGu0diPJ15GjjAYk3mWf1WlubkRoMyikEX_Hh8iM0mE38A9ZYVPVXkbMjBYXNCc') };
                  const pushSubscription = await registration.pushManager.subscribe(options);
                  if (navigator.onLine && pathname != "/" && pathname != "/login" && pathname != "/password/reset" && pathname != "/email/verify" && pathname != "/password/reset" + pathname.substring(pathname.lastIndexOf("/"))) {
                    let token = document.querySelector('meta[name=csrf-token]').getAttribute('content');
                    fetch('/notificacion-push', {
                      method: 'POST',
                      body: JSON.stringify(pushSubscription),
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'X-CSRF-Token': token
                      }
                    }).then((response) => {
                      return response.json();
                    });
                  };
                });
            };
        });
    }
  } else {
    //Notification isn't supported on this browser
    console.log("Este navegador no es compatible con las notificaciones de escritorio");
  };

  function urlBase64ToUint8Array(base64String) {
    var padding = '='.repeat((4 - base64String.length % 4) % 4);
    var base64 = (base64String + padding).replace(/\-/g, '+').replace(/_/g, '/');
    var rawData = window.atob(base64);
    var outputArray = new Uint8Array(rawData.length);
    for (var i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
  };
})()