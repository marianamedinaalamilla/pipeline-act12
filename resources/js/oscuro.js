(function() {
    "use strict";

    const body = document.querySelector('body'),
      modeSwitch = body.querySelector(".toggle-switch"),
      modeText = body.querySelector(".mode-text"),
      modeIMG = body.querySelector(".mode-img");

    modeSwitch.addEventListener("click" , () =>{
        body.classList.toggle("dark");
        if(body.classList.contains("dark")){
            modeText.innerText = "Modo Luz";
            modeIMG.src = 'img/logo uteams modo oscuro.png';
        }else{
            modeText.innerText = "Modo \n Oscuro";
            modeIMG.src = 'img/logo uteams.png';
        };
    });
  })()