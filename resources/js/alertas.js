(function() {
    "use strict";

    Livewire.on('modalCerrar', () => {
        $('#modal').modal('hide');
    });

    Livewire.on('registroGuardado', () => {
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'El registro se guardó con éxito.',
            showConfirmButton: false,
            timer: 1500
        })
    });

    Livewire.on('actualizarRegistro', () => {
        Swal.fire({
            title: '¿Estás seguro?',
            text: "Este registro se actualizará en la base de datos.",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '¡Sí, eliminar!',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {
                Livewire.emitTo('users', 'update');
                Livewire.emitTo('cuatrimestres', 'update');
                Livewire.emitTo('carreras', 'update');
                Livewire.emitTo('carrera-cuatrimestres', 'update');
                Livewire.emitTo('aulas', 'update');
                Livewire.emitTo('asignaturas', 'update');
                Livewire.emitTo('plan-estudios', 'update');
                Livewire.emitTo('grupos', 'update');
                Livewire.emitTo('grupo-asignaturas', 'update');
                Livewire.emitTo('sensores', 'update');
                Livewire.emitTo('sensor-grupos', 'update');
                Livewire.emitTo('docentes', 'update');
                Livewire.emitTo('docente-asignaturas', 'update');
                Livewire.emitTo('estudiantes', 'update');
                Livewire.emitTo('estudiante-grupos', 'update');
                Livewire.emitTo('asistencias', 'update');
                Livewire.emitTo('notificaciones', 'update');
            }
        })
    });

    Livewire.on('registroActualizado', () => {
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'El registro se actualizó con éxito.',
            showConfirmButton: false,
            timer: 1500
        })
    });

    Livewire.on('eliminarRegistro', (id) => {
        Swal.fire({
            title: '¿Estás seguro?',
            text: "Este registro se eliminará definitivamente de la base de datos.",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '¡Sí, eliminar!',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {
                Livewire.emitTo('users', 'destroy', id);
                Livewire.emitTo('cuatrimestres', 'destroy', id);
                Livewire.emitTo('carreras', 'destroy', id);
                Livewire.emitTo('carrera-cuatrimestres', 'destroy', id);
                Livewire.emitTo('aulas', 'destroy', id);
                Livewire.emitTo('asignaturas', 'destroy', id);
                Livewire.emitTo('plan-estudios', 'destroy', id);
                Livewire.emitTo('grupos', 'destroy', id);
                Livewire.emitTo('grupo-asignaturas', 'destroy', id);
                Livewire.emitTo('sensores', 'destroy', id);
                Livewire.emitTo('sensor-grupos', 'destroy', id);
                Livewire.emitTo('docentes', 'destroy', id);
                Livewire.emitTo('docente-asignaturas', 'destroy', id);
                Livewire.emitTo('estudiantes', 'destroy', id);
                Livewire.emitTo('estudiante-grupos', 'destroy', id);
                Livewire.emitTo('asistencias', 'destroy', id);
                Livewire.emitTo('notificaciones', 'destroy', id);
            }
        })
    });

    Livewire.on('registroEliminado', () => {
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'El registro se eliminó con éxito.',
            showConfirmButton: false,
            timer: 1500
        })
    });

    Livewire.on('registroError', () => {
        Swal.fire({
            position: 'top-end',
            icon: 'warning',
            title: 'El registro no se puede eliminar',
            showConfirmButton: false,
            timer: 1500
        })
    });

    Livewire.on('actualizarImagen', () => {
        Swal.fire({
            title: '¿Estás seguro?',
            text: "Esta imagen del usuario se actualizará en el servidor.",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '¡Sí, eliminar!',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {
                Livewire.emitTo('users', 'image');
                Livewire.emitTo('sensor-grupos', 'image');
            }
        })
    });

    Livewire.on('imagenActualizado', () => {
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'La imagen del usuario se actualizó con éxito.',
            showConfirmButton: false,
            timer: 1500
        })
    });
  })()