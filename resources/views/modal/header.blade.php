<div wire:ignore.self class="modal fade" id="modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="etiquetaModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content bg-white text-dark">
            <div class="modal-header">
                <h4 class="modal-title" id="etiquetaModal">
                    <b>{{ $tituloModulo }} </b> | {{ $identificador > 0 ? 'Editar' : 'Crear' }}
                </h4>
                <button type="button" wire:click.prevent="cancel()" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
           <div class="modal-body">