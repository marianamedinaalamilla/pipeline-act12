            </div>
            <div class="modal-footer">
                <button type="button" wire:click.prevent="cancel()" class="btn btn-danger" data-bs-dismiss="modal">{{ __('Salir') }}</button>
                @if ($boton && $identificador > 0)
                    <button type="button" wire:click.prevent="$emit('actualizarImagen')" class="btn btn-success">{{ __('Imagen') }}</button>
                @endif
                @if ($identificador < 1)
                    <button type="button" wire:click.prevent="$emit('agregarRegistro')" class="btn btn-primary">{{ __('Guardar') }}</button>
                @else
                    <button type="button" wire:click.prevent="$emit('actualizarRegistro')" class="btn btn-primary">{{ __('Actualizar') }}</button>
                @endif
            </div>
        </div>
    </div>
</div>