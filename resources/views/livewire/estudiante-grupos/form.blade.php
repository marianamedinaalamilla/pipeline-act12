@include('modal.header')
<form method="post" enctype="multipart/form-data">
    <div wire:loading class="alert alert-warning text-center" role="alert">
        <i class="bi bi-exclamation-diamond-fill"></i>
        {{ __('Por favor espere') }}
    </div>
    <div class="row">
        <div class="form-floating mb-4">
            <select wire:model.lazy="selectEstudiante" class="form-select bg-light text-dark border-0 @error('selectEstudiante') is-invalid @enderror" required>
                @if ($estudiantes->count())
                    <option selected>{{ __('Selecciona un estudiante') }}</option>
                    @foreach($estudiantes as $item)
                        <option value="{{ $item->id }}">{{ $item->user->apellido_paterno }} {{ __(' ') }} {{ $item->user->apellido_materno }} {{ __(' ') }} {{ $item->user->nombre }}</option>
                    @endforeach
                @else
                    <option selected>{{ __('No hay ningun estudiante') }}</option>
                @endif
            </select>
            <label for="selectEstudiante" class="text-dark">{{ __('Estudiantes') }}</label>
            @error('selectCarrera')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
        @if (!is_null($grupo2))
            <div class="form-floating mb-4">
                <select wire:model.lazy="grupo_id" class="form-select bg-light text-dark border-0 @error('grupo_id') is-invalid @enderror" required>
                @if ($grupo2->count())
                    <option selected>{{ __('Selecciona un grupo') }}</option>
                    @foreach($grupo2 as $item)
                        <option value="{{ $item->id }}">{{ $item->grupo }}</option>
                    @endforeach
                @else
                    <option selected>{{ __('No hay ningun grupo') }}</option>
                @endif
                </select>
                <label for="grupo_id" class="text-dark">{{ __('Grupos') }}</label>
                @error('grupo_id')
                    <span class="invalid-feedback" role="alert">
                        <label>{{ $message }}</label>
                    </span>
                @enderror
            </div>
        @endif
    </div>
</form>
@include('modal.footer')