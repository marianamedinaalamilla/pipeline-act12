@section('title', __('Sensores/Grupos'))
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col mb-2">
							<a style="text-decoration: none; color: black;" href="{{ url('/sensores') }}">{{ __('Sensores') }}</a>{{ __('/') }}<b>{{ __('Grupos') }}</b>
						</div>
					</div>
					<div class="row">
						<div class="mb-2">
							<span class="h2 text-dark float-left fw-bold">
								<i class="bx bx-group icon"></i>
								{{ __('Grupos') }}
								<small>
									<button class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Agregar grupo para los sensores">
										<i class="bx bxs-plus-circle bx-xs" aria-hidden="true"></i>
									</button>
								</small>
							</span>
						</div>
					</div>
					<div class="row">
						<div class="d-flex flex-xxl-row justify-content-between flex-column">
							@if($sensores->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroSensor" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona un sensor') }}</option>
										@foreach($sensores as $item)
											<option value="{{ $item->id }}">{{ $item->modelo }}</option>
										@endforeach
									</select>
									<label for="filtroGenero">{{ __('Sensores') }}</label>
								</div>
							@endif
							@if($grupos->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroGrupo" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona un grupo') }}</option>
										@foreach($grupos as $item)
											<option value="{{ $item->id }}">{{ $item->grupo }}</option>
										@endforeach
									</select>
									<label for="filtroRoles">{{ __('Grupos') }}</label>
								</div>
							@endif
							@if($modos->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroEstatus" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona un estatu') }}</option>
										@foreach($modos as $item)
											<option value="{{ $item->id }}">{{ $item->estatu }}</option>
										@endforeach
									</select>
									<label for="filtroEstatus">{{ __('Estatus') }}</label>
								</div>
							@endif
						</div>
					</div>
				</div>

				@include('livewire.sensor-grupos.form')

				@if($sensorGrupos->count())
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-borderless text-dark">
								<thead class="thead">
									<tr class="text-center"> 
										<th scope="col">{{ __('Foto') }}</th>
										<th scope="col">{{ __('Sensor') }}</th>
										<th scope="col">{{ __('Grupo') }}</th>
										<th scope="col">{{ __('Estatu') }}</th>
										<th scope="col">{{ __('Acciones') }}</th>
									</tr>
								</thead>
								<tbody>
									@foreach($sensorGrupos as $row)
									<tr>
										<td scope="row"><img class="img-fluid rounded-circle" src="{{ asset(''.$row->sensor->foto) }}" alt="{{ $row->sensor->modelo }}" width="110" height="110"></td>
										<td scope="row">{{ $row->sensor->modelo }}</td>
										<td scope="row">{{ $row->grupo->grupo }}</td>
										<td scope="row">{{ $row->estatuSensor->estatu }}</td>
										<td scope="row" class="text-center">
											<div class="py-2">
												<button wire:click="edit({{ $row->id }})" type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Editar el sensor {{ $row->sensor->modelo }} del grupo {{ $row->grupo->grupo }}">
													<i class="bx bxs-edit bx-xs" aria-hidden="true"></i>
												</button>
											</div>
											<div class="py-2">
												<button wire:click.prevent="$emit('eliminarRegistro', {{ $row->id }})" type="button" class="btn btn-danger btn-sm" data-bs-toggle="tooltip" title="Eliminar el sensor {{ $row->sensor->modelo }} del grupo {{ $row->grupo->grupo }}">
													<i class="bx bxs-trash bx-xs" aria-hidden="true"></i>
												</button>
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>						
							{{ $sensorGrupos->links() }}
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>