@include('modal.header')
<form method="post" enctype="multipart/form-data">
    <div wire:loading class="alert alert-warning text-center" role="alert">
        <i class="bi bi-exclamation-diamond-fill"></i>
        {{ __('Por favor espere') }}
    </div>
    <div class="row">
        <div class="form-floating mb-4">
            <select wire:model.lazy="grupo_id" class="form-select bg-light text-dark border-0 @error('grupo_id') is-invalid @enderror" required>
                @if($grupos->count())
                    <option selected>{{ __('Selecciona un grupo') }}</option>
                    @foreach($grupos as $item)
                        <option value="{{ $item->id }}">{{ $item->grupo }}</option>
                    @endforeach
                @else
                    <option selected>{{ __('No hay ningun grupo') }}</option>
                @endif
            </select>
            <label for="sensor_id" class="text-dark">{{ __('Grupos') }}</label>
            @error('grupo_id')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
        <div class="form-floating mb-4">
            <input wire:model.lazy="fecha" type="date" class="form-control bg-light text-dark border-0 @error('fecha') is-invalid @enderror" required>
            <label for="fecha" class="text-dark">{{ __('Fecha') }}</label>
            @error('fecha')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
        <div class="form-floating mb-4">
            <input wire:model.lazy="hora_abrir" type="time" class="form-control bg-light text-dark border-0 @error('hora_abrir') is-invalid @enderror" required>
            <label for="hora_abrir" class="text-dark">{{ __('Hora para abrir') }}</label>
            @error('hora_abrir')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
        <div class="form-floating mb-4">
            <input wire:model.lazy="hora_cerrar" type="time" class="form-control bg-light text-dark border-0 @error('hora_cerrar') is-invalid @enderror" required>
            <label for="hora_cerrar" class="text-dark">{{ __('Hora para cerrar') }}</label>
            @error('hora_cerrar')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
    </div>
</form>
@include('modal.footer')