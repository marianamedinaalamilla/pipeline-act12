@include('modal.header')
<form method="post" enctype="multipart/form-data">
    <div wire:loading class="alert alert-warning text-center" role="alert">
        <i class="bi bi-exclamation-diamond-fill"></i>
        {{ __('Por favor espere') }}
    </div>
    <div class="row">
        <div class="form-floating mb-4">
            <select wire:model.lazy="grado_id" class="form-select bg-light text-dark border-0 @error('grado_id') is-invalid @enderror" required>
                <option selected>{{ __('Selecciona un grado') }}</option>
                @foreach($grados as $item)
                <option value="{{ $item->id }}">{{ $item->grado }}</option>
                @endforeach
            </select>
            <label for="grado_id" class="text-dark">{{ __('Grados') }}</label>
            @error('grado_id')
            <span class="invalid-feedback" role="alert">
                <label>{{ $message }}</label>
            </span>
            @enderror
        </div>
        <div class="form-floating mb-4">
            <input wire:model.lazy="carrera" type="text" class="form-control bg-light text-dark border-0 @error('carrera') is-invalid @enderror" placeholder="Ingresa una carrera" required autocomplete="carrera">
            <label for="carrera" class="text-dark">{{ __('Carrera') }}</label>
            @error('carrera')
            <span class="invalid-feedback" role="alert">
                <label>{{ $message }}</label>
            </span>
            @enderror
        </div>
    </div>
</form>
@include('modal.footer')