@section('title', __('Carreras'))
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="mb-2">
							<span class="h2 text-dark float-left fw-bold">
								<i class="bi bi-mortarboard icon"></i>
								{{ __('Carreras') }}
								<small>
									<button class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Agregar carrera">
										<i class="bx bxs-plus-circle bx-xs" aria-hidden="true"></i>
									</button>
								</small>
							</span>
						</div>
					</div>
					<div class="row">
						<div class="d-flex flex-xxl-row justify-content-between flex-column">
							<div class="form-floating mb-2">
								<select wire:model.lazy="filtroGrado" class="form-select bg-light text-dark border-0">
									<option value="" selected>{{ __('Selecciona un grado') }}</option>
									@foreach($grados as $item)
										<option value="{{ $item->id }}">{{ $item->grado }}</option>
									@endforeach
								</select>
								<label for="filtroGrado">{{ __('Grados') }}</label>
							</div>
							<div class="form-floating mb-2">
								<input wire:model='buscador' class="form-control bg-light text-dark border-0" type="text" placeholder="Buscar carrera">
								<label for="buscador">{{ __('Buscar carrera') }}</label>
							</div> 
						</div>
					</div>
				</div>

				@include('livewire.carreras.form')

				@if($carreras->count())
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-borderless text-dark">
								<thead class="thead">
									<tr class="text-center"> 
										<th scope="col">{{ __('Grado') }}</th>
										<th scope="col">{{ __('Carrera') }}</th>
										<th scope="col">{{ __('Acciones') }}</th>
									</tr>
								</thead>
								<tbody>
									@foreach($carreras as $row)
									<tr>
										<td scope="row">{{ $row->grado->grado }}</td>
										<td scope="row">{{ $row->carrera }}</td>
										<td scope="row" class="text-center">
											<div class="py-2">
												<button wire:click="edit({{ $row->id }})" type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Editar a la carrera de {{ $row->carrera }}">
													<i class="bx bxs-edit bx-xs" aria-hidden="true"></i>
												</button>
											</div>
											<div class="py-2">
												<button wire:click.prevent="$emit('eliminarRegistro', {{ $row->id }})" type="button" class="btn btn-danger btn-sm" data-bs-toggle="tooltip" title="Eliminar a la carrera de {{ $row->carrera }}">
													<i class="bx bxs-trash bx-xs" aria-hidden="true"></i>
												</button>
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>						
							{{ $carreras->links() }}
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>