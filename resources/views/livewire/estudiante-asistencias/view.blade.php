@section('title', __('Asistencias de los estudiantes'))
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="mb-2">
							<span class="h2 text-dark float-left fw-bold">
								<i class="bi bi-card-checklist icon"></i>
								{{ __('Asistencias de los estudiantes') }}
							</span>
						</div>
					</div>
				</div>

				@if($estudianteAsistencias->count())
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-borderless text-dark">
								<thead class="thead">
									<tr class="text-center"> 
										
									</tr>
								</thead>
								<tbody>
									@foreach($estudianteAsistencias as $row)
									<tr>
										
									</tr>
									@endforeach
								</tbody>
							</table>						
							{{ $estudianteAsistencias->links() }}
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>