@include('modal.header')
<form method="post" enctype="multipart/form-data">
    <div wire:loading class="alert alert-warning text-center" role="alert">
        <i class="bi bi-exclamation-diamond-fill"></i>
        {{ __('Por favor espere') }}
    </div>
    <div class="row">
        <div class="form-floating col-lg-6 mb-4">
            <input wire:model.lazy="foto" type="file" accept="image/*" class="form-control bg-light text-dark border-0 @error('foto') is-invalid @enderror" id="{{ rand() }}" required>
            <label for="foto" class="text-dark">{{ __('Foto') }}</label>
            @error('foto')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
        @if ($foto && $identificador < 1)
        <div class="col-lg-6 mb-4 text-center">
            <img  src="{{ $foto->temporaryUrl() }}" class="img-fluid rounded-circle" alt="foto" width="110" height="110">            
        </div>
        @elseif ($foto && $identificador > 0)
        <div class="col-lg-6 mb-4 text-center">
            <img src="{{ $foto == $tmp ? asset('').$foto : $foto->temporaryUrl() }}" class="img-fluid rounded-circle" alt="{{ $modelo }}" width="110" height="110">
        </div>
        @endif
    </div>
    <div class="row">
        <div class="form-floating col-lg-6 mb-4">
            <input wire:model.lazy="modelo" type="text" class="form-control bg-light text-dark border-0 @error('modelo') is-invalid @enderror" placeholder="Ingresa el modelo" required autocomplete="modelo">
            <label for="modelo" class="text-dark">{{ __('Modelo') }}</label>
            @error('modelo')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
        <div class="form-floating col-lg-6 mb-4">
            <textarea wire:model.lazy="descripcion" class="form-control bg-light text-dark border-0 @error('modelo') is-invalid @enderror" placeholder="Ingresa una descripción" required></textarea>
            <label for="descripcion" class="text-dark">{{ __('Descripción') }}</label>
            @error('descripcion')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
    </div>
    
</form>
@include('modal.footer')