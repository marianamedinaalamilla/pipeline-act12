@section('title', __('Grupos/Asignaturas'))
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col mb-2">
							<a style="text-decoration: none; color: black;" href="{{ url('/grupos') }}">{{ __('Grupos') }}</a>{{ __('/') }}<b>{{ __('Asignaturas') }}</b>
						</div>
					</div>
					<div class="row">
						<div class="mb-2">
							<span class="h2 text-dark float-left fw-bold">
								<i class="bx bx-book icon"></i>
								{{ __('Asignaturas') }}
								<small>
									<button class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Agregar asignatura para los grupos">
										<i class="bx bxs-plus-circle bx-xs" aria-hidden="true"></i>
									</button>
								</small>
							</span>
						</div>
					</div>
					<div class="row">
						<div class="d-flex flex-xxl-row justify-content-between flex-column">
							@if ($grupos->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroGrupo" name="filtroGrupo" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona un grupo') }}</option>
										@foreach($grupos as $item)
											<option value="{{ $item->id }}">{{ $item->grupo }}</option>
										@endforeach
									</select>
									<label for="filtroGrupo">{{ __('Grupos') }}</label>
								</div>
							@endif
							@if ($asignaturas->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroAsignatura" name="filtroAsignatura" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona una asignatura') }}</option>
										@foreach($asignaturas as $item)
											<option value="{{ $item->id }}">{{ $item->asignatura }}</option>
										@endforeach
									</select>
									<label for="filtroAsignatura">{{ __('Asignaturas') }}</label>
								</div>
							@endif
							@if ($dias->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroDia" name="filtroDia" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona un día de la semana') }}</option>
										@foreach($dias as $item)
											<option value="{{ $item->id }}">{{ $item->dia }}</option>
										@endforeach
									</select>
									<label for="filtroDia">{{ __('Días de la semana') }}</label>
								</div>
							@endif 
						</div>
						<div class="d-flex flex-xxl-row justify-content-between flex-column">
							<div class="form-floating mb-2">
								<input wire:model='filtroHora' class="form-control bg-light text-dark border-0" type="time" placeholder="Buscar una hora">
								<label for="filtroHora">{{ __('Buscar una hora') }}</label>
							</div> 
						</div>
					</div>
				</div>

				@include('livewire.grupo-asignaturas.form')

				@if($grupoAsignaturas->count())
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-borderless text-dark">
								<thead class="thead">
									<tr class="text-center"> 
										<th scope="col">{{ __('Grupo') }}</th>
										<th scope="col">{{ __('Asignatura') }}</th>
										<th scope="col">{{ __('Día de la semana') }}</th>
										<th scope="col">{{ __('Hora de entrada') }}</th>
										<th scope="col">{{ __('Hora de salida') }}</th>
										<th scope="col">{{ __('Acciones') }}</th>
									</tr>
								</thead>
								<tbody>
									@foreach($grupoAsignaturas as $row)
									<tr>
										<td scope="row">{{ $row->grupo->grupo}}</td>
										<td scope="row">{{ $row->asignatura->asignatura }}</td>
										<td scope="row">{{ $row->dia->dia }}</td>
										<td scope="row">{{ $row->hora_entrada }}</td>
										<td scope="row">{{ $row->hora_salida }}</td>
										<td scope="row" class="text-center">
											<div class="py-2">
												<button wire:click="edit({{ $row->id }})" type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Editar la asignatura de {{ $row->asignatura->asignatura }} del grupo {{ $row->grupo->grupo }}">
													<i class="bx bxs-edit bx-xs" aria-hidden="true"></i>
												</button>
											</div>
											<div class="py-2">
												<button wire:click.prevent="$emit('eliminarRegistro', {{ $row->id }})" type="button" class="btn btn-danger btn-sm" data-bs-toggle="tooltip" title="Eliminar la asignatura de {{ $row->asignatura->asignatura }} del grupo {{ $row->grupo->grupo }}">
													<i class="bx bxs-trash bx-xs" aria-hidden="true"></i>
												</button>
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>						
							{{ $grupoAsignaturas->links() }}
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>