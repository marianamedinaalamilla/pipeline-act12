@include('modal.header')
<form method="post" enctype="multipart/form-data">
    <div wire:loading class="alert alert-warning text-center" role="alert">
        <i class="bi bi-exclamation-diamond-fill"></i>
        {{ __('Por favor espere') }}
    </div>
    <div class="row">
        <div class="form-floating mb-4">
            <select wire:model.lazy="selectGrupo" name="selectGrupo" class="form-select bg-light text-dark border-0 @error('selectGrupo') is-invalid @enderror" required>
                @if ($grupos->count())
                    <option selected>{{ __('Selecciona un grupo') }}</option>
                    @foreach($grupos as $item)
                        <option value="{{ $item->id }}">{{ $item->grupo }}</option>
                    @endforeach
                @else
                    <option selected>{{ __('No hay ningun grupo') }}</option>
                @endif
            </select>
            <label for="selectGrupo" class="text-dark">{{ __('Grupos') }}</label>
            @error('selectGrupo')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
    </div>
    @if (!is_null($asignaturas2))
        <div class="row">
            <div class="form-floating mb-4">
                <select wire:model.lazy="asignatura_id" class="form-select bg-light text-dark border-0 @error('asignatura_id') is-invalid @enderror" required>
                    @if ($asignaturas2->count())
                        <option selected>{{ __('Selecciona una asignatura') }}</option>
                        @foreach($asignaturas2 as $item)
                            <option value="{{ $item->id }}">{{ $item->asignatura }}</option>
                        @endforeach
                    @else
                        <option selected>{{ __('No hay ninguna asignatura') }}</option>
                    @endif
                </select>
                <label for="asignatura_id" class="text-dark">{{ __('Asignaturas') }}</label>
                @error('asignatura_id')
                    <span class="invalid-feedback" role="alert">
                        <label>{{ $message }}</label>
                    </span>
                @enderror
            </div>
        </div>
        @foreach ($dias as $item)
            <div class="row justify-content-center">
                <div class="form-check col-lg-2 mb-4">
                    <input wire:model.lazy="dia_id.{{ $item->id }}" value="{{ $item->id }}" type="checkbox" class="form-check-input @error('dia_id') is-invalid @enderror" required>
                    <label class="form-check-label text-dark" for="dia_id">{{ $item->dia }}</label>
                    @error('dia_id')
                        <span class="invalid-feedback" role="alert">
                            <label>{{ $message }}</label>
                        </span>
                    @enderror
                </div>
                <div class="form-floating col-lg-4 mb-4">
                    <input wire:model.lazy="hora_entrada.{{ $item->id }}" type="time" class="form-control bg-light text-dark border-0 @error('hora_entrada') is-invalid @enderror" required>
                    <label for="hora_entrada" class="text-dark">{{ __('Hora de entrada') }}</label>
                    @error('hora_entrada')
                        <span class="invalid-feedback" role="alert">
                            <label>{{ $message }}</label>
                        </span>
                    @enderror
                </div>
                <div class="form-floating col-lg-4 mb-4">
                    <input wire:model.lazy="hora_salida.{{ $item->id }}" type="time" class="form-control bg-light text-dark border-0 @error('hora_salida') is-invalid @enderror" required>
                    <label for="hora_salida" class="text-dark">{{ __('Hora de salida') }}</label>
                    @error('hora_salida')
                        <span class="invalid-feedback" role="alert">
                            <label>{{ $message }}</label>
                        </span>
                    @enderror
                </div>
            </div>
        @endforeach
    @endif
</form>
@include('modal.footer')