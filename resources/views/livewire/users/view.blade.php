@section('title', __('Usuarios'))
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="mb-2">
							<span class="h2 text-dark float-left fw-bold">
								<i class="bx bx-user icon"></i>
								{{ __('Usuarios') }}
								<small>
									<button class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Agregar usuario">
										<i class="bx bxs-plus-circle bx-xs" aria-hidden="true"></i>
									</button>
								</small>
							</span>
						</div>
					</div>
					<div class="row">
						<div class="d-flex flex-xxl-row justify-content-between flex-column">
							@if($generos->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroGenero" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona un género') }}</option>
										@foreach($generos as $item)
											<option value="{{ $item->id }}">{{ $item->genero }}</option>
										@endforeach
									</select>
									<label for="filtroGenero">{{ __('Géneros') }}</label>
								</div>
							@endif
							@if($roles->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroRoles" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona un rol') }}</option>
										@foreach($roles as $item)
											@if ($item->guard_name == "web")
												<option value="{{ $item->id }}">{{ $item->name }}</option>
											@endif
										@endforeach
									</select>
									<label for="filtroRoles">{{ __('Roles') }}</label>
								</div>
							@endif
							@if($modos->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroEstatus" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona un estatu') }}</option>
										@foreach($modos as $item)
											<option value="{{ $item->id }}">{{ $item->estatu }}</option>
										@endforeach
									</select>
									<label for="filtroEstatus">{{ __('Estatus') }}</label>
								</div>
							@endif
							<div class="form-floating mb-2">
								<input wire:model='buscador' class="form-control bg-light text-dark border-0" type="text" placeholder="Buscar usuario">
								<label for="buscador">{{ __('Buscar usuario') }}</label>
							</div> 
						</div>
					</div>
				</div>

				@include('livewire.users.form')

				@if($users->count())
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-borderless text-dark">
								<thead class="thead">
									<tr class="text-center"> 
										<th scope="col">{{ __('Foto de perfil') }}</th>
										<th scope="col">{{ __('Apellido paterno') }}</th>
										<th scope="col">{{ __('Apellido materno') }}</th>
										<th scope="col">{{ __('Nombre') }}</th>
										<th scope="col">{{ __('Matricula') }}</th>
										<th scope="col">{{ __('Email') }}</th>
										<th scope="col">{{ __('Rol') }}</th>
										<th scope="col">{{ __('Genero') }}</th>
										<th scope="col">{{ __('Verificado') }}</th>
										<th scope="col">{{ __('Estatu') }}</th>
										<th scope="col">{{ __('Acciones') }}</th>
									</tr>
								</thead>
								<tbody>
									@foreach($users as $row)
									<tr>
										<td scope="row"><img class="img-fluid rounded-circle" src="{{ asset(''.$row->foto_perfil) }}" alt="{{ $row->username }}" width="110" height="110"></td>
										<td scope="row">{{ $row->apellido_paterno }}</td>
										<td scope="row">{{ $row->apellido_materno }}</td>
										<td scope="row">{{ $row->nombre }}</td>
										<td scope="row">{{ $row->username }}</td>
										<td scope="row">{{ $row->email }}</td>
										<td scope="row">
											<div class="table-responsive">
												<table class="table table-borderless text-dark">
													<thead class="thead">
														<tr class="text-center"> 
															@foreach ($row->roles->pluck('guard_name') as $guard)
																<th scope="col">{{ $guard }}</th>
															@endforeach
														</tr>
													</thead>
													<tbody>
														<tr class="text-center">
															@foreach ($row->getRoleNames() as $rol)
																<td scope="row">{{ $rol }}</td>
															@endforeach
														</tr>
													</tbody>
												</table>						
											</div>
										</td>
										<td scope="row">{{ $row->genero->genero }}</td>
										<td scope="row">
											@if ($row->email_verified_at)
												<div class="alert alert-success text-center" role="alert">
													{{ __('Sí') }}
												</div>
											@else
												<div class="alert alert-warning text-center" role="alert">
													{{ __('No') }}
												</div>
											@endif
											</td>
										<td scope="row">
										@if ($row->estatuUsuario->estatu)
											<div class="alert alert-success text-center" role="alert">
												{{ $row->estatuUsuario->estatu }}
											</div>
										@else
											<div class="alert alert-danger text-center" role="alert">
												{{ $row->estatuUsuario->estatu }}
											</div>
										@endif
										</td>
										<td scope="row" class="text-center">
											<div class="py-2">
												<button wire:click="edit({{ $row->id }})" type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Editar al usuario {{ $row->username }}">
													<i class="bx bxs-edit bx-xs" aria-hidden="true"></i>
												</button>
											</div>
											<div class="py-2">
												<button wire:click.prevent="$emit('eliminarRegistro', {{ $row->id }})" type="button" class="btn btn-danger btn-sm" data-bs-toggle="tooltip" title="Eliminar al usuario {{ $row->username }}">
													<i class="bx bxs-trash bx-xs" aria-hidden="true"></i>
												</button>
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>						
							{{ $users->links() }}
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>