@section('title', __('Aulas'))
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="mb-2">
							<span class="h2 text-dark float-left fw-bold">
								<i class="icon">
									<img style="background-color: white;" class="img-fluid" src="{{ asset('img/iconos/aulas.png') }}" alt="aulas">
								</i>
								{{ __('Aulas') }}
								<small>
									<button class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Agregar aula">
										<i class="bx bxs-plus-circle bx-xs" aria-hidden="true"></i>
									</button>
								</small>
							</span>
						</div>
					</div>
					<div class="row">
						<div class="d-flex flex-xxl-row justify-content-between flex-column">
							@if ($aulas2->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroAula" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona una aula') }}</option>
										@foreach($aulas2 as $item)
											<option value="{{ $item->aula }}">{{ $item->aula }}</option>
										@endforeach
									</select>
									<label for="filtroAula">{{ __('Aulas') }}</label>
								</div>
							@endif
							<div class="form-floating mb-2">
								<input wire:model='buscador' class="form-control bg-light text-dark border-0" type="text" placeholder="Buscar departamento">
								<label for="buscador">{{ __('Buscar departamento') }}</label>
							</div> 
						</div>
					</div>
				</div>

				@include('livewire.aulas.form')

				@if($aulas->count())
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-borderless text-dark">
								<thead class="thead">
									<tr class="text-center"> 
										<th scope="col">{{ __('Aula') }}</th>
										<th scope="col">{{ __('Departamento') }}</th>
										<th scope="col">{{ __('Acciones') }}</th>
									</tr>
								</thead>
								<tbody>
									@foreach($aulas as $row)
									<tr>
										<td scope="row">{{ $row->aula }}</td>
										<td scope="row">{{ $row->departamento }}</td>
										<td scope="row" class="text-center">
											<div class="py-2">
												<button wire:click="edit({{ $row->id }})" type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Editar a la aula {{ $row->aula }}">
													<i class="bx bxs-edit bx-xs" aria-hidden="true"></i>
												</button>
											</div>
											<div class="py-2">
												<button wire:click.prevent="$emit('eliminarRegistro', {{ $row->id }})" type="button" class="btn btn-danger btn-sm" data-bs-toggle="tooltip" title="Eliminar a la aula {{ $row->aula }}">
													<i class="bx bxs-trash bx-xs" aria-hidden="true"></i>
												</button>
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>						
							{{ $aulas->links() }}
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>