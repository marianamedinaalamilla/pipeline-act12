@section('title', __('Grupos'))
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="mb-2">
							<span class="h2 text-dark float-left fw-bold">
								<i class="bx bx-group icon"></i>
								{{ __('Grupos') }}
								<small>
									<button class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Agregar grupo">
										<i class="bx bxs-plus-circle bx-xs" aria-hidden="true"></i>
									</button>
								</small>
							</span>
						</div>
					</div>
					<div class="row">
						<div class="d-flex flex-xxl-row justify-content-between flex-column">
							@if ($carreras->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroCarrera" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona una carrera') }}</option>
										@foreach($carreras as $item)
											<option value="{{ $item->id }}">{{ $item->grado->abreviatura }} {{ __('en') }} {{ $item->carrera }}</option>
										@endforeach
									</select>
									<label for="filtroCarrera">{{ __('Carreras') }}</label>
								</div>
							@endif
							@if ($cuatrimestres->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroCuatrimestre" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona un cuatrimestre') }}</option>
										@foreach($cuatrimestres as $item)
											<option value="{{ $item->id }}">{{ $item->cuatrimestre }}</option>
										@endforeach
									</select>
									<label for="filtroCuatrimestre">{{ __('Cuatrimestres') }}</label>
								</div>
							@endif
							@if ($aulas->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroAula" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona una aula') }}</option>
										@foreach($aulas as $item)
											<option value="{{ $item->id }}">{{ $item->aula }}</option>
										@endforeach
									</select>
									<label for="filtroAula">{{ __('Aulas') }}</label>
								</div>
							@endif
						</div>
						<div class="d-flex flex-xxl-row justify-content-between flex-column">
							@if ($turnos->count())
								<div class="form-floating mb-2">
									<select wire:model.lazy="filtroTurno" class="form-select bg-light text-dark border-0">
										<option value="" selected>{{ __('Selecciona un turno') }}</option>
										@foreach($turnos as $item)
											<option value="{{$item->id}}">{{$item->turno}}</option>
										@endforeach
									</select>
									<label for="filtroTurno">{{ __('Turnos') }}</label>
								</div>
							@endif
							<div class="form-floating mb-2">
								<input wire:model='buscador' class="form-control bg-light text-dark border-0" type="text" placeholder="Buscar grupo">
								<label for="buscador">{{ __('Buscar grupo') }}</label>
							</div> 
						</div>
					</div>
				</div>

				@include('livewire.grupos.form')

				@if($grupos->count())
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-borderless text-dark">
								<thead class="thead">
									<tr class="text-center"> 
										<th scope="col">{{ __('Carrera') }}</th>
										<th scope="col">{{ __('Cuatrimestre') }}</th>
										<th scope="col">{{ __('Grupo') }}</th>
										<th scope="col">{{ __('Aula') }}</th>
										<th scope="col">{{ __('Turno') }}</th>
										<th scope="col">{{ __('Acciones') }}</th>
									</tr>
								</thead>
								<tbody>
									@foreach($grupos as $row)
									<tr>
										<td scope="row">{{ $row->carrera->grado->abreviatura }} {{ __('en') }} {{ $row->carrera->carrera }}</td>
										<td scope="row">{{ $row->cuatrimestre->cuatrimestre }}</td>
										<td scope="row">{{ $row->grupo }}</td>
										<td scope="row">{{ $row->aula->aula }}</td>
										<td scope="row">{{ $row->turno->turno }}</td>
										<td scope="row" class="text-center">
											<div class="py-2">
												<button wire:click="edit({{ $row->id }})" type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#modal" title="Editar el grupo de {{ $row->grupo }}">
													<i class="bx bxs-edit bx-xs" aria-hidden="true"></i>
												</button>
											</div>
											<div class="py-2">
												<button wire:click.prevent="$emit('eliminarRegistro', {{ $row->id }})" type="button" class="btn btn-danger btn-sm" data-bs-toggle="tooltip" title="Eliminar el grupo de {{ $row->grupo }}">
													<i class="bx bxs-trash bx-xs" aria-hidden="true"></i>
												</button>
											</div>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>						
							{{ $grupos->links() }}
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>