@include('modal.header')
<form method="post" enctype="multipart/form-data">
    <div wire:loading class="alert alert-warning text-center" role="alert">
        <i class="bi bi-exclamation-diamond-fill"></i>
        {{ __('Por favor espere') }}
    </div>
    <div class="row">
        <div class="form-floating col-lg-6 mb-4">
            <select wire:model.lazy="selectCarrera" class="form-select bg-light text-dark border-0 @error('selectCarrera') is-invalid @enderror" required>
                @if ($carreras->count())
                    <option selected>{{ __('Selecciona una carrera') }}</option>
                    @foreach($carreras as $item)
                        <option value="{{ $item->id }}">{{ $item->grado->abreviatura }} {{ __('en') }} {{ $item->carrera }}</option>
                    @endforeach
                @else
                    <option selected>{{ __('No hay ninguna carrera') }}</option>
                @endif
            </select>
            <label for="selectCarrera" class="text-dark">{{ __('Carreras') }}</label>
            @error('selectCarrera')
                <span class="invalid-feedback" role="alert">
                    <label>{{ $message }}</label>
                </span>
            @enderror
        </div>
        @if (!is_null($cuatrimestres2))
            <div class="form-floating col-lg-6 mb-4">
                <select wire:model.lazy="cuatrimestre_id" class="form-select bg-light text-dark border-0 @error('cuatrimestre_id') is-invalid @enderror" required>
                @if ($cuatrimestres2->count())
                    <option selected>{{ __('Selecciona un cuatrimestre') }}</option>
                    @foreach($cuatrimestres2 as $item)
                        <option value="{{ $item->cuatrimestre_id }}">{{ $item->cuatrimestre->cuatrimestre }}</option>
                    @endforeach
                @else
                    <option selected>{{ __('No hay ningun cuatrimestre') }}</option>
                @endif
                </select>
                <label for="cuatrimestre_id" class="text-dark">{{ __('Cuatrimestres') }}</label>
                @error('cuatrimestre_id')
                    <span class="invalid-feedback" role="alert">
                        <label>{{ $message }}</label>
                    </span>
                @enderror
            </div>
            @if ($cuatrimestres2->count())
                <div class="form-floating col-lg-6 mb-4">
                    <input wire:model.lazy="grupo" type="text" class="form-control bg-light text-dark border-0 @error('grupo') is-invalid @enderror" placeholder="Ingresa un grupo" required autocomplete="grupo">
                    <label for="grupo" class="text-dark">{{ __('Grupo') }}</label>
                    @error('grupo')
                        <span class="invalid-feedback" role="alert">
                            <label>{{ $message }}</label>
                        </span>
                    @enderror
                </div>
                <div class="form-floating col-lg-6 mb-4">
                    <select wire:model.lazy="aula_id" class="form-select bg-light text-dark border-0 @error('aula_id') is-invalid @enderror" required>
                    @if ($aulas->count())
                        <option selected>{{ __('Selecciona una aula') }}</option>
                        @foreach($aulas as $item)
                            <option value="{{ $item->id }}">{{ $item->aula }}</option>
                        @endforeach
                    @else
                        <option selected>{{ __('No hay ninguna aula') }}</option>
                    @endif
                    </select>
                    <label for="aula_id" class="text-dark">{{ __('Aulas') }}</label>
                    @error('aula_id')
                        <span class="invalid-feedback" role="alert">
                            <label>{{ $message }}</label>
                        </span>
                    @enderror
                </div>
                <div class="form-floating col-lg-6 mb-4">
                    <select wire:model.lazy="turno_id" class="form-select bg-light text-dark border-0 @error('turno_id') is-invalid @enderror" required>
                    <option selected>{{ __('Selecciona un turno') }}</option>
                    @foreach($turnos as $item)
                        <option value="{{ $item->id }}">{{ $item->turno }}</option>
                    @endforeach
                    </select>
                    <label for="turno_id" class="text-dark">{{ __('Turnos') }}</label>
                    @error('turno_id')
                        <span class="invalid-feedback" role="alert">
                            <label>{{ $message }}</label>
                        </span>
                    @enderror
                </div>
            @endif
        @endif
    </div>
</form>
@include('modal.footer')