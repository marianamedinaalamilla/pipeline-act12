@extends('layouts.sesion')

<!-- Title -->
@section('title')	
<title>{{ ('Cambiar contraseña') }}</title>
@endsection

@section('content')
<span class="padding-bottom--15">{{ __('Cambiar contraseña') }}</span>
<form id="stripe-login" method="post" action="{{ route('password.update') }}" enctype="multipart/form-data">
	@csrf
    <input type="hidden" name="token" value="{{ $token }}">
	<div class="field padding-bottom--24">
		<label for="email">{{ __('Email') }}</label>
		<input type="text" name="email" id="email" class="form-control @error('email') is-invalid @enderror" placeholder="Ingresa un email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
		@error('email')
		<span class="invalid-feedback" role="alert">
			<label>{{ $message }}</label>
		</span>
		@enderror
	</div>
    <div class="field padding-bottom--24">
        <label for="password">{{ __('Contraseña') }}</label>
		<input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror" placeholder="*****************" required autocomplete="new-password">
		@error('password')
		<span class="invalid-feedback" role="alert">
			<label>{{ $message }}</label>
		</span>
		@enderror
	</div>
    <div class="field padding-bottom--24">
        <label for="password-confirm">{{ __('Confirmar contraseña') }}</label>
		<input type="password" name="password_confirmation" id="password-confirm" class="form-control" placeholder="*****************" required autocomplete="new-password">
	</div>
	<div class="field padding-bottom--24">
		<input type="submit" name="enviar" value="Restablecer contraseña">
	</div>
</form>
@endsection