@extends('layouts.sesion')

<!-- Title -->
@section('title')	
<title>{{ ('Restablecer contraseña') }}</title>
@endsection

@section('content')
<span class="padding-bottom--15">{{ __('Restablecer contraseña') }}</span>
<form id="stripe-login" method="post" action="{{ route('password.email') }}" enctype="multipart/form-data">
	@csrf
	<div class="field padding-bottom--24">
		<label for="email">{{ __('Email') }}</label>
		<input type="text" name="email" id="email" class="form-control @error('email') is-invalid @enderror" placeholder="Ingresa un email" value="{{ old('email') }}" required autocomplete="email" autofocus>
		@error('email')
		<span class="invalid-feedback" role="alert">
			<label>{{ $message }}</label>
		</span>
		@enderror
	</div>
    <div class="field padding-bottom--24">
		<div class="grid--50-50">
			<label></label>
			<div class="reset-pass">
				<a href="{{ route('login') }}">{{ __('Iniciar Sesion') }}</a>
			</div>
		</div>
	</div>
	<div class="field padding-bottom--24">
		<input type="submit" name="enviar" value="Enviar">
	</div>
</form>

@section('js')	
@if(Session('status'))
<script type="text/javascript">
    Swal.fire(
		'¡Advertencia!',
		'{{ session("status") }}',
		'question'
    )
</script>
@endif
@endsection

@endsection