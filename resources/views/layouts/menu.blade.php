<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <!-- Mobile Specific Metas -->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta name="keywords" content="HTML, CSS, JavaScript, PHP">
        <meta name="author" content="Shock the System">
        
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <!-- Title -->
        <title>@hasSection('title') @yield('title') | @endif {{ config('app.name', 'UTeams') }}</title>
        
        <!-- Favicons -->
        <link href="{{ asset('img/logos/512x512_claro_uteams.png') }}" rel="apple-touch-startup-image">  
        
        <!-- Vendor CSS Files -->
        <link href="{{ asset('css/app.css') }}" type="text/css" rel="stylesheet">
        @laravelPWA
        @livewireStyles

        <!-- Template Main CSS File -->
        <link href="{{ asset('css/menu.css') }}" type="text/css" rel="stylesheet">
    </head>
    
    <body>
        <nav class="sidebar close">
            <header>
                <div class="image-text">
                    <span class="image">
                        <img class="mode-img img-fluid" src="{{ asset('img/logos/32x32_claro_uteams.png') }}" alt="UTeams">
                    </span>
    
                    <div class="text logo-text">
                        <span class="name">{{ __('eams') }}</span>
                        <span class="profession">{{ __('Aplicación WEB') }}</span>
                    </div>
                </div>
    
                <i class="bx bx-chevron-right toggle"></i>
                
            </header>
            <div class="menu-bar">
                <div class="menu">       
                    <li>
                        <img class="img-fluid rounded-circle icon" src="{{ asset(Auth::user()->foto_perfil) }}" alt="{{ Auth::user()->username }}"> 
                        <span class="text nav-text ms-2">{{ Auth::user()->username }}</span>
                    </li>
                    
                    <li class="search-box">
                        <i class="bx bx-search icon"></i>
                        <input type="text" placeholder="Buscar...">
                    </li>
    
                    <ul class="menu-links">
                        <li class="nav-link">
                            <a href="{{ route('home') }}">
                                <i class="bx bx-home-alt icon"></i>
                                <span class="text nav-text">{{ __('Inicio') }}</span>
                            </a>
                        </li>
                        <li class="nav-link">
                            <a href="{{ route('telescope') }}">
                                <i class="bx bx-bug icon"></i>
                                <span class="text nav-text">{{ __('Telescope') }}</span>
                            </a>
                        </li>
                        <li class="nav-link">
                            <a href="{{ url('/usuarios') }}">
                                <i class="bx bx-user icon"></i>
                                <span class="text nav-text">{{ __('Usuarios') }}</span>
                            </a>
                        </li>
                        <li class="nav-link">
                            <a href="{{ url('/cuatrimestres') }}">
                            <i class="icon">
                                <img style="background-color: white;" class="img-fluid" src="{{ asset('img/iconos/cuatrimestres.png') }}" alt="cuatrimestres">
                            </i>
                                <span class="text nav-text">{{ __('Cuatrimestres') }}</span>
                            </a>
                        </li>
                        <li class="nav-link dropup">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="bi bi-mortarboard icon"></i>
                                <span class="text nav-text">{{ __('Carreras') }}</span>
                            </a>
                            <ul class="dropdown-menu menu-links" style="background-color: #EAEAEA;">
                                <li class="nav-link">
                                    <a href="{{ url('/carreras') }}">
                                        <i class="bi bi-mortarboard icon"></i>
                                        <span class="text nav-text">{{ __('Carreras') }}</span>
                                    </a>
                                </li>
                                <li class="nav-link">
                                    <a href="{{ url('/carrera-cuatrimestres') }}">
                                        <i class="icon">
                                            <img style="background-color: white;" class="img-fluid" src="{{ asset('img/iconos/cuatrimestres.png') }}" alt="cuatrimestres">
                                        </i>
                                        <span class="text nav-text">{{ __('Cuatrimestres') }}</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-link">
                            <a href="{{ url('/aulas') }}">
                            <i class="icon">
                                <img style="background-color: white;" class="img-fluid" src="{{ asset('img/iconos/aulas.png') }}" alt="aulas">
                            </i>
                                <span class="text nav-text">{{ __('Aulas') }}</span>
                            </a>
                        </li>
                        <li class="nav-link">
                            <a href="{{ url('/asignaturas') }}">
                                <i class="bx bx-book icon"></i>
                                <span class="text nav-text">{{ __('Asignaturas') }}</span>
                            </a>
                        </li>
                        <li class="nav-link dropup">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="bx bx-group icon"></i>
                                <span class="text nav-text">{{ __('Grupos') }}</span>
                            </a>
                            <ul class="dropdown-menu menu-links" style="background-color: #EAEAEA;">
                                <li class="nav-link">
                                    <a href="{{ url('/grupos') }}">
                                        <i class="bx bx-group icon"></i>
                                        <span class="text nav-text">{{ __('Grupos') }}</span>
                                    </a>
                                </li>
                                <li class="nav-link">
                                    <a href="{{ url('/grupo-asignaturas') }}">
                                        <i class="bx bx-book icon"></i>
                                        <span class="text nav-text">{{ __('Asignaturas') }}</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-link dropup">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="icon">
                                    <img style="background-color: white;" class="img-fluid" src="{{ asset('img/iconos/rfid.png') }}" alt="sensores">
                                </i>
                                <span class="text nav-text">{{ __('Sensores') }}</span>
                            </a>
                            <ul class="dropdown-menu menu-links" style="background-color: #EAEAEA;">
                                <li class="nav-link">
                                    <a href="{{ url('/sensores') }}">
                                        <i class="icon">
                                            <img style="background-color: white;" class="img-fluid" src="{{ asset('img/iconos/rfid.png') }}" alt="sensores">
                                        </i>
                                        <span class="text nav-text">{{ __('Sensores') }}</span>
                                    </a>
                                </li>
                                <li class="nav-link">
                                    <a href="{{ url('/sensor-grupos') }}">
                                        <i class="bx bx-group icon"></i>
                                        <span class="text nav-text">{{ __('Grupos') }}</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-link dropup">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="bi bi-person-lines-fill icon"></i>
                                <span class="text nav-text">{{ __('Docentes') }}</span>
                            </a>
                            <ul class="dropdown-menu menu-links" style="background-color: #EAEAEA;">
                                <li class="nav-link">
                                    <a href="{{ url('/docentes') }}">
                                        <i class="bi bi-person-lines-fill icon"></i>
                                        <span class="text nav-text">{{ __('Docentes') }}</span>
                                    </a>
                                </li>
                                <li class="nav-link">
                                    <a href="{{ url('/docente-asignaturas') }}">
                                        <i class="bx bx-book icon"></i>
                                        <span class="text nav-text">{{ __('Asignaturas') }}</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-link dropup">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="bi bi-person-badge icon"></i>
                                <span class="text nav-text">{{ __('Estudiantes') }}</span>
                            </a>
                            <ul class="dropdown-menu menu-links" style="background-color: #EAEAEA;">
                                <li class="nav-link">
                                    <a href="{{ url('/estudiantes') }}">
                                        <i class="bi bi-person-badge icon"></i>
                                        <span class="text nav-text">{{ __('Estudiantes') }}</span>
                                    </a>
                                </li>
                                <li class="nav-link">
                                    <a href="{{ url('/estudiante-grupos') }}">
                                        <i class="bx bx-group icon"></i>
                                        <span class="text nav-text">{{ __('Grupos') }}</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-link">
                            <a href="{{ url('/estudiante-asistencias') }}">
                                <i class="bi bi-card-checklist icon"></i>
                                <span class="text nav-text">{{ __('Asistencias') }}</span>
                            </a>
                        </li>
                        <li class="nav-link">
                            <a href="{{ url('/notificacion-aulas') }}">
                                <i class="bi bi-bell icon"></i>
                                <span class="text nav-text">{{ __('Notificaciones') }}</span>
                            </a>
                        </li>
                    </ul>
                </div>
    
                <div class="bottom-content">
                    <li class="nav-link">
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="bx bx-log-out icon"></i>
                            <span class="text nav-text">{{ __('Cerrar Sesión') }}</span>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
							@csrf
						</form>
                    </li>

                    <li class="mode d-none">
                        <div class="sun-moon">
                            <i class="bx bx-moon icon moon"></i>
                            <i class="bx bx-sun icon sun"></i>
                        </div>
                        <span class="mode-text text">{{ __('Modo') }} <br> {{ __('Oscuro') }}</span>
                        <div class="toggle-switch">
                            <span class="switch"></span>
                        </div>
                    </li>
                </div>
            </div>
    
        </nav>
    
        <main class="home py-5">
                @yield('content')
        </main>
    </body>
    <!-- Vendor JS Files -->
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>	
    <script type="text/javascript" src="{{ asset('vendor/pouchdb/pouchdb.min.js') }}"></script>
    @livewireScripts

    <!-- Template UTeams JS File -->
	<script type="text/javascript" src="{{ asset('js/menu.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/alertas.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/offline.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bd.js') }}"></script>
    @yield('js')
</html>
